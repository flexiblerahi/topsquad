<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ContestUser extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'contest_id', 'match_id'];

    public function contest()
    {
        return $this->belongsTo(Contest::class, 'contest_id');
    }

    public function match()
    {
        return $this->belongsTo(MatchTeam::class, 'match_id');
    }

    public function league(): HasMany
    {
        return $this->hasMany(League::class, 'id', 'league_id');
    }

    public function teamone(): HasMany
    {
        return $this->hasMany(Team::class, 'id', 'team_one_id');
    }

    public function teamtwo(): HasMany
    {
        return $this->hasMany(Team::class, 'id', 'team_two_id');
    }

    public function leagueOne(): HasOne
    {
        return $this->hasOne(League::class, 'id', 'league_id');
    }

    public function teamoneOne(): HasOne
    {
        return $this->hasOne(Team::class, 'id', 'team_one_id');
    }

    public function teamtwoOne(): HasOne
    {
        return $this->hasOne(Team::class, 'id', 'team_two_id');
    }
}
