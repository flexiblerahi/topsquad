<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    use HasFactory;
    protected $dates = ['end_at'];
    protected $fillable = ['code', 'user_id', 'coin', 'end_at', 'status'];
}
