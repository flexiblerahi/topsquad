<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContestTeam extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'user_id', 'contest_id', 'captain', 'vice_captain', 'players', 'match_team_id'];

    public function captains()
    {
        return $this->belongsTo(Player::class, 'contest_id');
    }

    public function viceCaptains()
    {
        return $this->belongsTo(Player::class, 'vice_captain');
    }

    public function matchTeam()
    {
        return $this->belongsTo(MatchTeam::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
