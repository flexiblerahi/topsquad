<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Slider extends Model implements HasMedia
{
    use HasFactory , InteractsWithMedia;


    protected $fillable = [
        'name',
        'status',
        'image',
      ];

      public function registerMediaCollections(): void
      {
          $this->addMediaCollection('sliderImage')->singleFile();
      }


}
