<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchTeam extends Model
{
    use HasFactory;
    use SoftDeletes;

    const UPCOMING = 0, LIVE = 1, COMPLETED = 2;
    protected $dates = ['match_started_at'];

    protected $fillable = ['league_id', 'team_one_id', 'team_two_id', 'match_started_at', 'status','team_one_id',
    'team_one_run',
    'team_one_wicket',
    'team_one_over',
    'team_two_id',
    'team_two_run',
    'team_two_wicket',
    'team_two_over'];

    public function league(): HasMany
    {
        return $this->hasMany(League::class, 'id', 'league_id');
    }

    public function teamone(): HasMany
    {
        return $this->hasMany(Team::class, 'id', 'team_one_id');
    }

    public function teamtwo(): HasMany
    {
        return $this->hasMany(Team::class, 'id', 'team_two_id');
    }

    public function leagueOne(): HasOne
    {
        return $this->hasOne(League::class, 'id', 'league_id');
    }

    public function teamoneOne(): HasOne
    {
        return $this->hasOne(Team::class, 'id', 'team_one_id');
    }

    public function teamtwoOne(): HasOne
    {
        return $this->hasOne(Team::class, 'id', 'team_two_id');
    }
}
