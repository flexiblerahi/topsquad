<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContestRank extends Model
{
    use HasFactory;
    protected $fillable = ['contest_rank_id', 'position', 'user_id', 'totalpoints', 'contest_id', 'match_team_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
