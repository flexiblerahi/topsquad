<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Withdraw extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['user_id', 'amount', 'method', 'mobile', 'status', 'approve_at'];
    protected $dates = ['approve_at'];

    const PENDING = 0, APPROVE = 1, CANCEL = 2;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
