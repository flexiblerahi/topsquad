<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;
    const PENDING = 0, APPROVE = 1, CANCEL = 2;
    const METHODPROFILE = "profile";
    protected $dates = ['approve_at'];
    protected $fillable = ['transaction_id', 'admin_payroll_id', 'mobile', 'package_id', 'user_id', 'amount', 'status', 'approve_at','method'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
