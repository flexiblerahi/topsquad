<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class League extends Model  implements HasMedia

{
  use HasFactory, InteractsWithMedia, SoftDeletes;

  const APPROVE = 1, CANCEL = 0;
  protected $fillable = [
    'name',
    'image',
    'shortname',
    'status',
  ];

  public function teams()
  {
    return $this->belongsToMany(Team::class);
  }
}
