<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisterOffer extends Model
{
    use HasFactory;
    protected $fillable = ['coin', 'point', 'status'];
}
