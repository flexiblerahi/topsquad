<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contest extends Model
{
    use HasFactory;
    use SoftDeletes;
    const ENTRYFEETYPECOIN = "coin", ENTRYFEETYPEMONEY = "money";

    const PENDING = 0, APPROVE = 1, CANCEL = 2;

    protected $fillable = ['name',
     'shortname',
     'match_id',
     'entry_fee',
     'entry_fee_type',
     'status',
     'count'
    ];

    public function match()
    {
        return $this->belongsTo(MatchTeam::class, 'match_id');
    }

    public function ranks()
    {
        return $this->hasMany(ContestRank::class);
    }

    public function contestUser()
    {
        return $this->hasMany(ContestUser::class);
    }

    public function userContestRanks()
    {
        return $this->hasMany(UserContestRank::class);
    }
}
