<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use HasFactory, SoftDeletes;
    const BATSMAN = 1;
    const BOWLER = 2;
    const WICKETKEEPER = 3;
    const ALLROUNDER = 4;
    const VICECAPTAIN = 1.5;
    const CAPTAIN = 2;

    protected $fillable = [
        'name',
        'position',
        'point',
        'is_announce',
        'credit'
    ];

    public function teams(): BelongsToMany
    {
        return $this->belongsToMany(Team::class);
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }
}
