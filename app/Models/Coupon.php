<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'point',
        'code',
        'active_date',
        'finished_date'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_coupon');
    }
}
