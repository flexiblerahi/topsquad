<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueTeam extends Model
{
    use HasFactory;

    protected $fillable = [
        'league_id',
        'team_id_one',
        'team_id_two'

      ];


    public function leagues(){
        return $this->belongsTo(League::class ,'league_id');
    }

    public function teamOne(){
        return $this->belongsTo(Team::class, 'team_id_one');
      }


    public function teamTwo(){
        return $this->belongsTo(Team::class, 'team_id_two');
      }


}
