<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id' => ['required', 'unique:payments,transaction_id'],
            'package_id' => ['required', 'exists:packages,id'],
            'method' => ['required'],
            'mobile' => ['required'],
            'admin_payroll_id' => ['required', 'exists:admin_payrolls,id']
        ];
    }
}
