<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContestUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'shortname' => ['required'],
            'match_id' => ['required'],
            'entry_fee' => ['required', 'numeric'],
            'entry_fee_type' => ['required'],
            'status' => ['required', 'boolean']
        ];
    }
}
