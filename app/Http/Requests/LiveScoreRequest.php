<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LiveScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_one_run' => ['nullable', 'numeric'],
            'team_one_wicket' => ['nullable', 'numeric', 'between:0,10'],
            'team_one_over' => ['nullable', 'numeric', 'between:0,20'],
            'team_two_run' => ['nullable', 'numeric'],
            'team_two_wicket' => ['nullable', 'numeric', 'between:0,10'],
            'team_two_over' => ['nullable', 'numeric', 'between:0,20'],
        ];
    }
}
