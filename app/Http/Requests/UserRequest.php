<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['nullable','string','email','unique:users,email'],
            'username' => ['required','string','unique:users,username'],
            'password' => ['required','min:9','confirmed'],
            'phone' => ['required', 'unique:users,phone','digits:11'],
        ];
    }
}
