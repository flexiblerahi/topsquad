<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatchTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'league_id' => ['required'],
            'team_one_id' => ['required'],
            'team_two_id' => ['required', 'different:team_one_id'],
            'match_started_at' => ['required'],
            'status' => ['required', 'integer'],
        ];
    }
}
