<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;


class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => ['required', 'string'],
            'point' => ['required', 'numeric'],
            'active_date' =>  ['required', 'date', 'after:' . Carbon::now()],
            'finished_date' => ['required', 'date', 'after:active_date'],
        ];
    }
}
