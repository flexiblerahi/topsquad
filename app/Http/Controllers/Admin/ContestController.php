<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContestRequest;
use App\Http\Requests\ContestUpdateRequest;
use App\Models\MatchTeam;
use Carbon\Carbon;

class ContestController extends Controller
{
    public function index()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $contests = Contest::with('match')->whereRelation('match', 'match_teams.match_started_at', '>=', Carbon::now())
        ->with('match', fn ($query) => $query->where('match_teams.match_started_at', '>=', Carbon::now()))->paginate(10);
        return view('dashboard.contest.upcoming', compact('contests'));
    }

    public function create()
    {
        $matches = MatchTeam::with('teamoneOne', 'teamtwoOne')->where('status', MatchTeam::UPCOMING)->where('match_started_at', '>=', Carbon::now())->latest()->get();
        return view('dashboard.contest.create', compact('matches'));
    }

    public function store(ContestRequest $request)
    {
        $input = $request->validated();
        $input['entry_fee_type'] = 'coin';
        Contest::create($input);
        return back()->with('success', 'Contest Create Success');
    }

    public function edit(Contest $contest)
    {
        $matches = MatchTeam::with('teamoneOne', 'teamtwoOne')->where('status', MatchTeam::UPCOMING)->get();
        return view('dashboard.contest.edit', compact('contest', 'matches'));
    }

    public function update(ContestUpdateRequest $request, Contest $contest)
    {
        $input = $request->validated();
        $contest->update($input);
        return back()->with('success', 'Update Successfully');
    }

    public function destroy(Contest $contest)
    {
        $contest->delete();
        return back()->with('success', 'Delete Successfully');
    }


    public function history()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $contests = Contest::with('match')->whereRelation('match', 'match_teams.match_started_at', '<=', Carbon::now())
        ->with('match', fn ($query) => $query->where('match_teams.match_started_at', '<=', Carbon::now()))->paginate(10);
        return view('dashboard.contest.upcoming', compact('contests'));
    }

    public function running()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $contests = Contest::with('match')->whereRelation('match', 'match_teams.match_started_at', '<=', Carbon::now())
        ->whereRelation('match', 'match_teams.status', MatchTeam::LIVE)
        ->with('match', fn ($query) => $query->where('match_teams.status', MatchTeam::LIVE)->where('match_teams.match_started_at', '<=', Carbon::now()))->paginate(10);
        return view('dashboard.contest.running', compact('contests'));
    }
}
