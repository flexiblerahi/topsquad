<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders= Slider::with('media')->get();

        // dd($sliders);


         return view('dashboard.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'             => 'required|string',
            'Slider_imgae'             => 'required|image|mimes:jpeg,png,jpg|max:1024',
            'status' => 'boolean'

        ]);
        $input = $request->all();
        $slider = Slider::create($input);
        if($request->hasFile('Slider_imgae') ){
            $slider->addMediaFromRequest('Slider_imgae')->toMediaCollection('sliderImage');
        }

        return back()->with('success', 'Slider Created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $sliders= Slider::with('media')->where('id', $user)->first();
         return view('dashboard.slider.edit', compact('sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $request->validate([
            'name'             => 'required|string',
            'Slider_imgae'             => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'status' => 'boolean'

        ]);
        $slider = Slider::find($user);
        $slider->update([
            'name' =>  $request->input('name'),
            'status' =>  $request->input('status'),

        ]);

        if($request->hasFile('Slider_imgae') ){
            $slider->addMediaFromRequest('Slider_imgae')->toMediaCollection('sliderImage');
        }

        return back()->with('success', 'Slider Updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $slider = Slider::find($user);
        $slider->delete();

         return back()->with('success', 'Slider Deleted successfully.');
    }
}
