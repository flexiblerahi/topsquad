<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContestTeam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contest;
use App\Models\MatchTeam;
use App\Models\Player;
use App\Models\User;
use App\Models\UserContestRank;
use Carbon\Carbon;

class ContestTeamController extends Controller
{
    public function complete($match_team_id)
    {
        $msg = '';
        $contestTeams = ContestTeam::where('match_team_id', $match_team_id)->get();
        $groupContestTeams = $contestTeams->groupby('contest_id');
        foreach ($groupContestTeams as $groupContestTeam) {
            $msg = $this->groupByContest($groupContestTeam);
        }
        MatchTeam::whereId($match_team_id)->update(['status' => MatchTeam::COMPLETED]);
        return back()->with('success', $msg);
    }

    public function groupByContest($contestTeams)
    {
        $teamArray = collect();
        $contest = Contest::find($contestTeams->first()->contest_id);
        $contest->load('ranks');
        $contestRanks = $contest->ranks; //get ranks data
        if (count($contestRanks) == 0) return 'Ranks Not found.'; //if in an contest rank not found
        foreach ($contestTeams as $key => $contestTeam) { // to calculate total point of players for each users
            $totalPoint = 0;
            $players = Player::whereIn('id', json_decode($contestTeam->players))->get(['id', 'point']);
            foreach ($players as $player) {
                if ($contestTeam->captain == $player->id) {
                    $totalPoint = $totalPoint + ($player->point * 2); // if player position is captain then captain point multiply by 2
                } else if ($contestTeam->vice_captain == $player->id) {
                    $totalPoint = $totalPoint + ($player->point * 1.5); // if player position is vice captain then captain point multiply by 1.5
                } else $totalPoint = $totalPoint + $player->point;
            }
            $contestTeam->totalPoint = $totalPoint;
        }
        $shortedTeams = $contestTeams->sortByDesc('totalPoint'); // short by total point
        foreach ($shortedTeams as $shorted) {
            $matches = $shortedTeams->where('totalPoint', $shorted->totalPoint);
            if (count($matches) > 1 && !($teamArray->contains('id', $shorted->id))) { // if more than users earn same total points
                $shortedCreatedAts = $matches->sortBy('created_at'); //then short by which user join contest first
                foreach ($shortedCreatedAts as $shortedCreatedAt) {
                    $teamArray->push($shortedCreatedAt); //all created_at shorted data store in one collection
                }
            }
            if (!($teamArray->contains('id', $shorted->id))) $teamArray->push($shorted); //all total point shorted data store in one collection
        }
        $position = 0;
        $userContestRankList = array();
        foreach ($teamArray as $key => $team) {
            $position++;
            $user = User::find($team->user_id);
            if (count($contestRanks) > $key) {
                $user->increment('money', $contestRanks[$key]->amount);
                $user->save();
            }
            $userContestRankList[] = array(
                'contest_rank_id' => count($contestRanks) > $key ? $contestRanks[$key]->id : null,
                'user_id' => $user->id,
                'position' => $position,
                'totalpoints' => $team->totalPoint,
                'isrank' => count($contestRanks) > $key ? 1 : 0,
                'contest_id' => $team->contest_id,
                'match_team_id' => $team->match_team_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        }
        UserContestRank::insert($userContestRankList);
        return 'save Successfully';
    }

    public function users($contest_id)
    {
        $contest_teams = ContestTeam::with('user')->where('contest_id', $contest_id)->get();
        foreach ($contest_teams as $key => $contestTeam) { // to calculate total point of players for each users
            $totalPoint = 0;
            $players = Player::whereIn('id', json_decode($contestTeam->players))->get(['id', 'point']);
            foreach ($players as $player) {
                if ($contestTeam->captain == $player->id) {
                    $totalPoint = $totalPoint + ($player->point * 2); // if player position is captain then captain point multiply by 2
                } else if ($contestTeam->vice_captain == $player->id) {
                    $totalPoint = $totalPoint + ($player->point * 1.5); // if player position is vice captain then captain point multiply by 1.5
                } else $totalPoint = $totalPoint + $player->point;
            }
            $contestTeam->point = $totalPoint;
        }
        return view('dashboard.contest.users', compact('contest_teams'));
    }
}
