<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminPayroll;
use Illuminate\Http\Request;

class AdminPayrollController extends Controller
{
    public function index()
    {
        $payrolls = AdminPayroll::all();
        return view('dashboard.payroll.index', compact('payrolls'));
    }

    public function create()
    {
        return view('dashboard.payroll.create');
    }

    public function store(Request $request)
    {
        $input = $request->only('mobile', 'status', 'method');
        AdminPayroll::create($input);
        return back()->with('success', 'Payroll Insert Successfully');
    }

    public function edit(AdminPayroll $adminPayroll)
    {
        return view('dashboard.payroll.edit', compact('adminPayroll'));
    }

    public function update(Request $request, AdminPayroll $adminPayroll)
    {
        $input = $request->only('mobile', 'status', 'method');
        $adminPayroll->update($input);
        return back()->with('success', 'Payroll Update Successfully');
    }

    public function destroy(AdminPayroll $adminPayroll)
    {
        $this->delete($adminPayroll);
        return back()->with('success', 'Delete Payroll');
    }
}
