<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Http\Request;

class PushNotificationController extends Controller
{
    //

    public function index()
    {
        $notifiactions = Notification::latest()->paginate($this->itemPerPage);
        $this->putSL($notifiactions);
        return view('dashboard.notification.index', compact('notifiactions'));
    }

    public function create()
    {
        return view('dashboard.notification.create');
    }
    public function sendNotification(Request $request)
    {
        // $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        // dd($request->all());
        $SERVER_API_KEY = "AAAAN7jcDzk:APA91bEvuRrove7BTgFgO8tIO7oO4Gxp1Ov7vQCj-mbKRuhII7KTcm4icsRE7PDaspFtXlUDnebJFW8N-ufV-lMfoZOSDtVBskhk3yJ0vabppv8e9UUvpecFaUwDdiSujMiSgDKGIy6t";

        $notifications = Notification::create([
            'title' => $request->title,
            'body' => $request->body,

        ]);

        $data = [
            "to" => '/topics/notification',
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

        // dd($response);
        return back()->with('success', 'Notification sended');
    }

    public function destroy(Notification $notification)
    {
        $notification->delete();
        return back()->with('success', 'Delete Successfully');
    }
}
