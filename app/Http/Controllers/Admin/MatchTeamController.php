<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LiveScoreRequest;
use App\Http\Requests\MatchTeamRequest;
use App\Models\League;
use App\Models\MatchTeam;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MatchTeamController extends Controller
{
    public function index()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $matches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '>=', Carbon::now())->with('leagueOne', 'teamoneOne', 'teamtwoOne')->paginate(10);
        return view('dashboard.match.index', compact('matches'));
    }

    public function create(Request $request)
    {
        $leagues = League::with('teams')->get();
        return view('dashboard.match.create', compact('leagues'));
    }

    public function store(MatchTeamRequest $request)
    {
        $input = $request->validated();
        MatchTeam::create($input);
        return back()->with('success', 'Match Schedule Successfully');
    }

    public function edit(MatchTeam $matchTeam)
    {

        $leagues = League::with('teams')->get();
        return view('dashboard.match.edit', compact('leagues', 'matchTeam'));
    }

    public function update(MatchTeamRequest $request, MatchTeam $matchTeam)
    {
        $input = $request->validated();
        $matchTeam->update($input);
        return redirect()->route('match_team.index')->with('success', 'Update Successfully');
    }

    public function destroy(MatchTeam $matchTeam)
    {
        $matchTeam->delete();
        return back()->with('success', 'Delete Successfully');
    }

    public function live()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $matches = MatchTeam::where('status', MatchTeam::LIVE)->where('match_started_at', '<=', Carbon::now())->with('leagueOne', 'teamoneOne', 'teamtwoOne')->paginate(10);
        return view('dashboard.match.live', compact('matches'));
    }

    public function announcePlayers(MatchTeam $match_team)
    {
        $match_team->load('teamoneOne');
        $match_team->load('teamtwoOne');
        $match_team->teamoneOne->load('players');
        $match_team->teamtwoOne->load('players');
        return view('dashboard.match.announce', compact('match_team'));
    }

    public function livescore(MatchTeam $match_team)
    {
        $match_team->load('teamoneOne');
        $match_team->load('teamtwoOne');
        return view('dashboard.match.livescore', compact('match_team'));
    }

    public function livescoreStore(LiveScoreRequest $request, MatchTeam $match_team)
    {
        $input = $request->validated();
        $match_team->update($input);
        return back()->with('success', 'Update Successfully');
    }

    public function completed()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $matches = MatchTeam::where('status', MatchTeam::COMPLETED)->where('match_started_at', '<=', Carbon::now())->with('leagueOne', 'teamoneOne', 'teamtwoOne')->paginate(10);
        return view('dashboard.match.complete', compact('matches'));
    }
}
