<?php

namespace App\Http\Controllers\Admin;

use App\Models\Payment;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\User;
use Carbon\Carbon;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::with('user')->where('status', Payment::PENDING)->paginate(5);
        return view('dashboard.payment.index', compact('payments'));
    }

    public function edit(Payment $payment)
    {
        $package = Package::find($payment->package_id);
        $user = User::find($payment->user_id);
        if($payment->method == "profile") {
            if($user->money <= $package->money) return back()->with('success', 'Insuffient Balance');
            $user->decrement('money', $package->money);
        }
        $user->increment('coin', $package->coin);
        $user->save();
        $payment->update([
            'status' => Payment::APPROVE,
            'approve_at' =>  Carbon::now()
        ]);
        return back()->with('success', 'Payment Approved Successfully');
    }

    public function cancel(Payment $payment)
    {
        $payment->update([
            'status' => Payment::CANCEL,
            'approve_at' =>  Carbon::now()
        ]);
        return back()->with('success', 'Payment Cancel Successfully');
    }

    public function approve()
    {
        $payments = Payment::with('user')->where('status', Payment::APPROVE)->paginate(5);
        return view('dashboard.payment.approve', compact('payments'));
    }

    public function cancelHistory()
    {
        $payments = Payment::with('user')->where('status', Payment::CANCEL)->paginate(5);
        return view('dashboard.payment.cancel', compact('payments'));
    }

    public function destroy(Payment $payment)
    {
        $payment->delete();
        return back()->with('success', 'Payment delete successfully');
    }

}
