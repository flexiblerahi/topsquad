<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;

class PackageController extends Controller
{
    public function index()
    {
        $packages = Package::paginate(10);
        return view('dashboard.package.index', compact('packages'));
    }

    public function create()
    {
        return view('dashboard.package.create');
    }

    public function store(PackageRequest $request)
    {
        $input = $request->validated();
        Package::create($input);
        return back()->with('success', 'Package Create Successfully');
    }

    public function edit(Package $package)
    {
        return view('dashboard.package.edit', compact('package'));
    }

    public function update(PackageRequest $request, Package $package)
    {
        $input = $request->validated();
        $package->update($input);
        return back()->with('success', 'Package Update Successfully');
    }

    public function destroy(Package $package)
    {
        $package->delete();
        return back()->with('success', 'Package Delete Successfully');
    }
}
