<?php

namespace App\Http\Controllers\Admin;

use App\Models\Player;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlayerRequest;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function create($team_id, $position)
    {
        return view('dashboard.player.create', compact('team_id', 'position'));
    }

    public function store(PlayerRequest $request, $team_id)
    {
        $input = $request->validated();
        $input['is_announce'] = isset($input['is_announce']) ? 1 : 0;
        $player = Player::create($input);
        $player->teams()->attach($team_id);
        return back()->with('success', 'Player Insert Successfully');
    }

    public function edit(Player $player)
    {
        $player->load('teams');
        $team = $player->teams->first();
        return view('dashboard.player.edit', compact('player', 'team'));
    }

    public function update(PlayerRequest $request, Player $player)
    {
        $input = $request->validated();
        $input['is_announce'] = isset($input['is_announce']) ? 1 : 0;
        $input['point'] = isset($input['is_announce']) ? $player->point : 0;
        $player->update($input);
        return back()->with('success', 'Player Insert Successfully');
    }

    public function destroy(Player $player)
    {
        $player->delete();
        return back()->with('success', 'Player Delete successfully');
    }

    public function updateplayers(Request $request)
    {
        foreach ($request->all() as $key => $point) {
            $id = explode("-",$key);
            if($id[0] != "_token") {
                Player::where('id', $id[1])->update(['point' => $point]);
            }
        };
        return back()->with('success', 'Player point update successfully');
    }
}
