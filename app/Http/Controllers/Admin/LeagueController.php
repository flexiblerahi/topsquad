<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\LeagueTeamResource;
use App\Models\League;
use Illuminate\Http\Request;

class LeagueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leagues = League::all();
        return view('dashboard.league.index', compact('leagues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.league.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|string',
            'status' => 'boolean'

        ]);
        $input = $request->all();
        $league = League::create($input);

        return back()->with('success', 'League Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, League $league)
    {
        if($request->ajax()) {
            $league->load('teams');
            return $this->apiResponseResourceCollection(200, 'All teams under League', LeagueTeamResource::make($league));
        } 
        $league->load('teams');
        return view('dashboard.league.view', compact('league'));
    }

    public function dropdown(Request $request)
    {
        dd('hello how are');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(League $league)
    {
        // $leagues= League::find($league);
        return view('dashboard.league.edit', compact('league'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $league)
    {
        $request->validate([
            'name'       => 'required|string',
            'status'     => 'boolean'

        ]);
        $leagues = League::find($league);
        $leagues->update([
            'name' =>  $request->input('name'),
            'status' =>  $request->input('status'),

        ]);
        return back()->with('success', 'teams Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($league)
    {
        $leagues = League::find($league);
        $leagues->delete();

        return back()->with('success', 'league Deleted successfully.');
    }
}
