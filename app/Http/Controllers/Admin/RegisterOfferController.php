<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterOfferRequest;
use App\Models\RegisterOffer;

class RegisterOfferController extends Controller
{
    public function create()
    {
        $register_offer = RegisterOffer::first();
        if(empty($register_offer)) return view('dashboard.offer.create');
        else return view('dashboard.offer.edit', compact('register_offer'));
    }

    public function store(RegisterOfferRequest $request)
    {
        $input = $request->validated();
        $input['status'] = $request->input('status') ? 1 : 0;
        RegisterOffer::create($input);
        return back()->with('success', 'Offer Create Successful');
    }

    public function update(RegisterOfferRequest $request, RegisterOffer $registerOffer)
    {
        $input = $request->validated();
        $input['status'] = $request->input('status') ? 1 : 0;
        $registerOffer->update($input);
        return back()->with('success', 'Update Register Offer Successfully');
    }
}
