<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Withdraw;
use Carbon\Carbon;

class WithdrawController extends Controller
{
    public function index()
    {
        $pending_requests = Withdraw::where('status', Withdraw::PENDING)->with('user')->latest()->get();
        return view('dashboard.withdraw.index', compact('pending_requests'));
    }

    public function approve()
    {
        $approve_requests = Withdraw::whereIn('status', [Withdraw::APPROVE, Withdraw::CANCEL])->with('user')->latest()->get();
        return view('dashboard.withdraw.approve', compact('approve_requests'));
    }

    public function update(Request $request, Withdraw $withdraw)
    {
        $user = User::find($request->input('user_id'));
        if($user->money <= $withdraw->amount) return back()->with('success', 'Insufficient Balance');
        $user->decrement('money', $withdraw->amount);
        $user->save();
        $withdraw->update([
            'status' => Withdraw::APPROVE,
            'approve_at' => Carbon::now()
        ]);
        return back()->with('success', 'Withdraw approved');
    }

    public function cancel(Withdraw $withdraw)
    {
        $withdraw->update([
            'status' => Withdraw::CANCEL,
            'approve_at' => Carbon::now()
        ]);
        return back()->with('success', 'Withdraw cancel');
    }
}
