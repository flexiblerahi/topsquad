<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeamRequest;
use App\Http\Requests\TeamUpdateRequest;
use App\Http\Resources\TeamResource;
use App\Models\League;
use App\Models\Team;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class TeamController extends Controller
{
   
    public function index()
    {
        $teams = Team::paginate(10);
        return view('dashboard.team.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leagues = League::all();
        return view('dashboard.team.create', compact('leagues'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $input = $request->validated();
        $input['image'] = uploadFile($input['image'], 'team');
        $input['shortname'] = strtoupper($input['shortname']);
        $team = Team::create($input);
        $team->leagues()->attach($input['league_id']);
        return back()->with('success', 'Team Created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        $team->with('players');
        return view('dashboard.player.index', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $leagues = League::all();
        return view('dashboard.team.edit', compact('team', 'leagues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamUpdateRequest $request,Team $team)
    {
        $input = $request->validated();
        $team->image = $request->image != $team->image ? uploadFile($request->image, 'team') : $request->image;
        $team->name = $input['name'];
        $team->status = $input['status'];
        $team->leagues->first()->pivot->league_id = $input['league_id'];
        $team->leagues->first()->pivot->save();
        $team->shortname = strtoupper($input['shortname']);
        $team->save();
        return back()->with('success', 'Team Updated.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($team)
    {
        $team = Team::find($team);
        $team->delete();
        return back()->with('success', 'Slider Deleted successfully.');
    }
}
