<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Nullable;

class UserController extends Controller
{
    public function index()
    {
        dd(auth()->user());
        $users = User::whereHas('roles', function ($query) {
            $query->where('roles.name', 'Super Admin');
        }, '<', 1)->get();

        // dd($users);



        return view('dashboard.user.index', compact('users'));
    }

    public function create()
    {
        return view('dashboard.user.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $request->validate(
            [

                'email' => 'nullable|string|email|unique:users,email',
                'username' => 'required|string|unique:users,username',
                'password' => 'required||min:9|confirmed',
                'password_confirmation' => 'required|min:9',
                'phone' => 'required|digits:11'

            ],
            // [
            //     'email.required' => 'The :attribute field can not be blank value.',
            //     'username.required' => 'The :attribute field can not be blank value.',
            //     'password.required' => 'The :attribute field can not be blank value.',
            //     'phone.required' => 'The :attribute field can not be blank value.',
            //     'password_confirmation.required' => 'The :attribute field does not match .',

            // ],

        );

        // dd('ss');
        // dd($validator['email']);


        $user = User::create([
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('user.index')->with('success', 'New User created successfully.');

        // return back()->with('success', 'Slider Created.');
    }

    public function show($id)
    {
    }

    public function edit($user)
    {
        $this->checkPermission('profile.access');


        $users = User::find($user);

        return view('dashboard.user.edit', compact('users'));
    }

    public function update(Request $request, $user)
    {
        $validator = $request->validate(
            [
                'name' => 'required|string|max:255',
                'email' => 'nullable|string|email|unique:users,email,' . $user,
                'username' => 'required|string|unique:users,username,' . $user,
                'phone' => 'required|digits:11',
                'password' => 'nullable|min:8|confirmed'
            ]
        );

        $user = User::find($user);


        $user->update([
            'name' => $request->first_name,
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'password' => $request->password ? Hash::make($request->password) : $user->password,
        ]);


        return back()->with('success', 'User update successfully.');
    }

    public function destroy($user)
    {
        $user = User::find($user);
        $user->delete();
        return back()->with('success', 'User Deleted successfully.');
    }

    public function editProfile()
    {
        $this->checkPermission('profile.edit');
        $user = auth()->user();

        return view('dashboard.user.edit-profile', compact('user'));
    }


    public function change_password()
    {
        $this->checkPermission('profile.edit');
        return view('dashboard.user.change_password');
    }
}
