<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContestRank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contest;

class ContestRankController extends Controller
{
    public function index($contest_id)
    {

        $ranks = ContestRank::where('contest_id', $contest_id)->get();
        return view('dashboard.contest-rank.index', compact('ranks', 'contest_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($contest_id)
    {
        return view('dashboard.contest.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $contest_id)
    {
        $rank = ContestRank::where('contest_id', $contest_id)->get()->last();
        $input['amount'] = $request->input('amount');
        $rank == null ? $input['ranks'] = 1 : $input['ranks'] = increment($rank->ranks);
        $input['contest_id'] = $contest_id;
        $input['amount_type'] = 'money';
        ContestRank::create($input);
        return back()->with('success', 'Contest Rank Create Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function show(ContestRank $contestRank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function edit(ContestRank $contestRank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContestRank $contestRank)
    {
        $contestRank->amount = $request->input('amount');
        $contestRank->save();
        return back()->with('success', 'Rank Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContestRank $contestRank)
    {
        $contestRank->delete();
        return back()->with('success', 'Rank Delete successfully');
    }
}
