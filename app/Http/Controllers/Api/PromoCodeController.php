<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PromoCodeResource;
use App\Models\Coupon;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $notifications = Notification::paginate($this->itemPerPage);
        // return $this->apiResponseResourceCollection(201, 'Notification list', PromoCodeResource::collection($notifications));
        return $this->apiResponseResourceCollection(
            200,
            'Notice List',
            PromoCodeResource::collection(
                Notification::latest()
                    ->paginate(10)
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $code = $request->code;
        $coupon_details = Coupon::where([['code', $code], ['finished_date', '>=', Carbon::now()]])->first();
        if (!empty($coupon_details)) {

            $coupon_used = auth()->user()->coupon->where('code', $code)->first();
            if (!empty($coupon_used)) {
                return $this->apiResponse(201, 'Already Applied');
            }
            $point['coin'] = $coupon_details->point + auth()->user()->coin;
            // dd($point);
            User::find(auth()->id())->coupon()->attach($coupon_details->id);
            auth()->user()->update($point);

            return $this->apiResponse(201, 'Promo Applied');
        }

        return $this->apiResponse(201, 'Code does not match');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
