<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PivotResource;
use App\Http\Resources\PlayerTeamResource;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{

    public function index(): JsonResponse
    {
        $teams = Team::all();
        return $this->apiResponseResourceCollection(200, 'All Teams', TeamResource::collection($teams));
    }

    public function show(Team $team): JsonResponse
    {
        $team->load('players');
        return $this->apiResponseResourceCollection(200, 'Players Under Team', PlayerTeamResource::make($team));
    }
}
