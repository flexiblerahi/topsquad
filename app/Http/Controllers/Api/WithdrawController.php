<?php

namespace App\Http\Controllers\Api;

use App\Models\Withdraw;
use App\Http\Controllers\Controller;
use App\Http\Requests\WithdrawRequest;
use App\Http\Resources\WithdrawResource;

class WithdrawController extends Controller
{
    public function index()
    {
        $withdraw_history = Withdraw::where('user_id', auth()->id())->get();
        return $this->apiResponseResourceCollection(201, 'Withdraw history', WithdrawResource::collection($withdraw_history));
    }

    public function store(WithdrawRequest $request)
    {
        $input = $request->validated();
        $input['user_id'] = auth()->id();

        if(auth()->user()->money >= $input['amount']) {
            Withdraw::create($input);
            return $this->apiResponse(201, 'Withdraw Succeessfully');
        }
        return $this->apiResponse(404, 'Insufficient Balance');
    }
}
