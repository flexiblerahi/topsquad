<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MatchTeamRequest;
use App\Http\Resources\MakeTeamResource;
use App\Http\Resources\MatchTeamResource;
use App\Models\League;
use App\Models\MatchTeam;
use Carbon\Carbon;

class MatchTeamController extends Controller
{
    public function index()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $matches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '>=', Carbon::now())->get();
        $matches->load('league');
        $matches->load('teamone');
        $matches->load('teamtwo');
        // dd($matches);
        return $this->apiResponseResourceCollection(200, 'all Leagues', MatchTeamResource::collection($matches));
    }

    public function create()
    {
        $leagues = League::with('teams')->get();
        return view('dashboard.match.create', compact('leagues'));
    }

    public function store(MatchTeamRequest $request)
    {
        $input = $request->validated();
        MatchTeam::create($input);
        return back()->with('success', 'Match Schedule Successfully');
    }

    public function createTeam($id)
    {
        $match = MatchTeam::find($id);
        $match->load('teamoneOne');
        $match->load('teamtwoOne');
        $match->teamoneOne->load('players');
        $match->teamtwoOne->load('players');
        // dd($match);
        return $this->apiResponseResourceCollection(200, 'make team player list', MakeTeamResource::make($match));

    }
}
