<?php

namespace App\Http\Controllers\Api;

use App\Actions\Fortify\ResetUserPassword;
use App\Http\Controllers\Controller;
use App\Mail\ForgetPasswordMail;
use App\Models\ForgetPassword;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordController extends Controller
{
    //

    public function sendForgetPassword(Request $request)
    {
        $emailValidate = $request->validate([
            'email'  => ['required'],
        ]);

        $data = array();

        $userEmail = User::where('email', $emailValidate)->first();

        if (!$userEmail) {
            return response()->json([
                'message' => 'These credentials do not match our records.'
            ], 404);
        }

        $data['code'] = random_int(100000, 999999);
        $data['user_id'] = $userEmail->id;
        // $data
        ForgetPassword::create($data);

        Mail::to($userEmail)->send(new ForgetPasswordMail($data));
        return response()->json([
            'message' => 'mail send.'
        ], 200);
    }


    public function codeVerify(Request $request)
    {

        $codeValidate = $request->validate([
            'code'  => ['required'],
        ]);

        $code = ForgetPassword::find($codeValidate);
        if (!$code) {
            return response()->json([
                'message' => 'Code does not match.'
            ], 404);
        }

        return response()->json([
            'message' => 'code verify.'
        ], 200);
    }

    public function forgetPassword(Request $request, ResetUserPassword $reset)
    {
        $emailValidate = $request->validate([
            'email'  => ['required'],
        ]);

        $user = User::where('email', $emailValidate)->first();

        event(new PasswordReset($password = $reset->reset($user, $request->all())));

        return response()->json([
            'message' => 'Password Changed'
        ], 200);
    }
}
