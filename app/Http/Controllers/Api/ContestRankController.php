<?php

namespace App\Http\Controllers\Api;

use App\Models\ContestRank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContestRankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function show(ContestRank $contestRank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function edit(ContestRank $contestRank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContestRank $contestRank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContestRank  $contestRank
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContestRank $contestRank)
    {
        //
    }
}
