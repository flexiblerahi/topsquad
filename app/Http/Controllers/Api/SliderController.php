<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SliderCollection;
use App\Http\Resources\SliderResource;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{



    public function index(){

        $slider = SliderResource::collection(Slider::with('media')->get()) ;

        return response()->json([
            'slider' => $slider,
            'message' => 'Slider List',
        ], 201);

    }
}
