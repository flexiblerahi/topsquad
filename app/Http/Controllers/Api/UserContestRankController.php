<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserContestRankResource;
use App\Models\Contest;
use App\Models\UserContestRank;
use Illuminate\Http\Request;

class UserContestRankController extends Controller
{
    public function index($contest_id)
    {

        $contest = Contest::find($contest_id);
        $contest->load('userContestRanks');
        foreach ($contest->userContestRanks  as $contestRank) $contestRank->load('user');
        $contest->load('match');
        $contest->match->load('teamoneOne');
        $contest->match->load('teamtwoOne');
        return $this->apiResponseResourceCollection(201, 'User Ccntest Ranks', UserContestRankResource::make($contest));
    }
}
