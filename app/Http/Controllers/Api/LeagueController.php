<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LeagueResource;
use App\Http\Resources\LeagueTeamResource;
use App\Models\League;
use Illuminate\Http\Request;

class LeagueController extends Controller
{

    public function index()
    {
        $leagues = League::all();
        return $this->apiResponseResourceCollection(200, 'all Leagues', LeagueResource::collection($leagues));
    }

    public function show(League $league)
    {
        $league->load('teams');
        return $this->apiResponseResourceCollection(200, 'All teams under League', LeagueTeamResource::make($league));
    }

    public function update(Request $request, League $league)
    {
        dd($request->all());
    }

    public function test(Request $request, League $league)
    {
        dd($request->all());
    }
}
