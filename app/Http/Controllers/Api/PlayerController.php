<?php

namespace App\Http\Controllers\Api;

use App\Models\Player;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlayerResource;
use Illuminate\Http\JsonResponse;

class PlayerController extends Controller
{
    public function index():JsonResponse
    {
        $players = Player::all();
        return $this->apiResponseResourceCollection(200, 'all Leagues', PlayerResource::collection($players));   
    }
}
