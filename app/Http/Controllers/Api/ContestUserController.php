<?php

namespace App\Http\Controllers\Api;

use App\Models\ContestUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MatchTeamResource;
use App\Http\Resources\MyContestResource;
use App\Models\Contest;
use App\Models\MatchTeam;
use App\Http\Resources\MyContestTeamResource;
use Carbon\Carbon;

class ContestUserController extends Controller
{
    public function index()
    {
        $updateMatches = MatchTeam::where('status', MatchTeam::UPCOMING)->where('match_started_at', '<=', Carbon::now())->update(['status'=> MatchTeam::LIVE]);
        $contest_users = ContestUser::where('user_id', auth()->id())->get();
        $contest_users->load('match');
        $allContestsUser = $this->allMyContests($contest_users);
        return $this->apiResponseResourceCollection(200, 'my contests', MyContestResource::make($allContestsUser));
    }

    public function allMyContests($contest_users)
    {
        $upcomming = array();
        $live = array();
        $completed = array();
        foreach ($contest_users as $contest_user) {
            if($contest_user->match->status == MatchTeam::UPCOMING) {
                $contest_user->match->status = 'upcoming';
                $upcomming[]['match'] = $contest_user->match;
            } elseif ($contest_user->match->status == MatchTeam::COMPLETED) {
                $contest_user->match->status = 'completed';
                $completed[]['match'] = $contest_user->match;
            } elseif ($contest_user->match->status == MatchTeam::LIVE) {
                $contest_user->match->status = 'live';
                $live[]['match'] = $contest_user->match;
            }
            $contest_user->match->match_started = setDateTime($contest_user->match->match_started_at);
            $contest_user->match->load('league');
            $contest_user->match->league[0]->image = setImage($contest_user->match->league[0]->image);
            $contest_user->match->load('teamone');
            $contest_user->match->teamone[0]->image = setImage($contest_user->match->teamone[0]->image);
            $contest_user->match->load('teamtwo');
            $contest_user->match->teamtwo[0]->image = setImage($contest_user->match->teamtwo[0]->image);
        }
        return array('upcoming'=> $upcomming, 'live'=> $live, 'completed' => $completed);
    }

    public function store(Request $request, $contest_id)
    {
        $input = array('contest_id'=> $contest_id, 'user_id'=> auth()->user()->id);
        dd(auth()->user());
    }

    public function myContestList($match_id)
    {
        $contest_users = ContestUser::where(['match_id' => $match_id, 'user_id' => auth()->id()])->latest()->get();
        $contest_users->load('contest');
        foreach ($contest_users as $contest_user) {
            $contest_user->contest->load('ranks');
            $contest_user->contest->load('contestUser');
        }
        return $this->apiResponseResourceCollection(200, 'all Contests', MyContestTeamResource::collection($contest_users));
    }
}
