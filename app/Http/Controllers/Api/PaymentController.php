<?php

namespace App\Http\Controllers\Api;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentMethodRequest;
use App\Http\Requests\PaymentRequest;
use App\Http\Resources\PaymentResource;
use App\Models\Package;
use Carbon\Carbon;

class PaymentController extends Controller
{
    public function manualPay(PaymentMethodRequest $request)
    {
        $input = $request->validated();
        $input['amount'] = Package::find($input['package_id'])->money;
        $input['user_id'] = auth()->id();
        Payment::create($input);
        return $this->apiResponse(201, 'Payment Request Send Successfully');
    }

    public function show()
    {
        $history = Payment::where('user_id', auth()->user()->id)->withTrashed()->get();
        return $this->apiResponseResourceCollection(200, 'Payment History list', PaymentResource::collection($history));
    }

    public function profilePay(Request $request)
    {
        $input = $request->validate([
            'package_id'  => ['required', 'exists:packages,id'],
        ]);
        $package = Package::find($input['package_id']);
        $user = auth()->user();
        if($user->money <= $package->money) return $this->apiResponse(404, 'Insufficient balance');
        $user->decrement('money', $package->money);
        $user->increment('coin', $package->coin);
        $user->save();
        $input['amount'] = $package->money;
        $input['user_id'] = $user->id;
        $input['method'] = Payment::METHODPROFILE;
        $input['status'] = Payment::APPROVE;
        $input['approve_at'] = Carbon::now();
        Payment::create($input);
        return $this->apiResponse(201, 'Payment Request Send Successfully');
    }
}
