<?php

namespace App\Http\Controllers\Api;

use App\Models\ContestTeam;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContestTeamRequest;
use App\Http\Resources\ContestMakeTeamResource;
use App\Http\Resources\ContestTeamPlayerResource;
use App\Http\Resources\ContestTeamResource;
use App\Models\Contest;
use App\Models\MatchTeam;
use App\Models\Player;
use Carbon\Carbon;

class ContestTeamController extends Controller
{
    public function index()
    {
        $teams = ContestTeam::where('user_id', auth()->user()->id)->get();
        return $this->apiResponseResourceCollection(201, 'Team list', ContestTeamResource::collection($teams));
    }

    public function store(ContestTeamRequest $request, $contest_id)
    {
        if(ContestTeam::where(['contest_id'=> $contest_id, 'user_id' => auth()->id()])->exists()) return $this->apiResponse(404, 'Already Team created for this contest');
        $input = $request->validated();
        $input['players'] = json_encode($input['players']);
        $contest = Contest::find($contest_id);
        if(auth()->user()->coin <= $contest->entry_fee) return $this->apiResponse(404, 'Insufficient Balance!!!');
        if($contest != null) {
            $contest->load('match');
            $input['contest_id'] = $contest->id;
            $input['match_team_id'] = $contest->match->id;
            $input['user_id'] = auth()->user()->id;
            ContestTeam::create($input);
            return $this->apiResponse(201, 'Contest Team Created Successfully');
        }
        return $this->apiResponse(404, 'Invalid Contest');
    }

    public function show($contest_id)
    {
        $contestTeam = ContestTeam::where(['contest_id' => $contest_id, 'user_id' => auth()->id()])->first();
        $playersId = json_decode($contestTeam->players);
        $contestTeam->players = Player::whereIn('id', $playersId)->get();
        $totalPoint = 0;
        foreach($contestTeam->players as $player) {
            $player->vice_captain = false;
            $player->captain = false;
            if($contestTeam->vice_captain == $player->id) {
                $player->point = $player->point * Player::VICECAPTAIN;
                $player->vice_captain = true ;
            }
            if($contestTeam->captain == $player->id) {
                $player->captain = true;
                $player->point = $player->point * Player::CAPTAIN;
            }
            $totalPoint = $totalPoint + $player->point;
            $contestTeam->totalPoint = $totalPoint;
        }
        return $this->apiResponseResourceCollection(201, 'player list', ContestTeamPlayerResource::make($contestTeam));
    }

    public function edit($contest_id)
    {
        $contestTeam = ContestTeam::where(['user_id' => auth()->id(), 'contest_id' => $contest_id])->first();
        $contestTeam->load('matchTeam');
        $contestTeam->matchTeam->load('teamoneOne');
        $contestTeam->matchTeam->load('teamtwoOne');
        $contestTeam->matchTeam->teamoneOne->load('players');
        $contestTeam->matchTeam->teamtwoOne->load('players');
        return $this->apiResponseResourceCollection(200, 'make team player list', ContestMakeTeamResource::make($contestTeam));
    }

    public function update(ContestTeamRequest $request, ContestTeam $contestTeam)
    {
        $contestTeam->load('matchTeam');

        if($contestTeam->matchTeam->match_started_at <= Carbon::now() or $contestTeam->matchTeam->status == MatchTeam::LIVE) return $this->apiResponse(404, 'Sorry!! Edit request not accepted.');

        $input = $request->validated();
        $contestTeam->name = $input['name'];
        $contestTeam->players = json_encode($input['players']);
        $contestTeam->captain = $input['captain'];
        $contestTeam->vice_captain = $input['vice_captain'];
        $contestTeam->save();
        return $this->apiResponse(200, 'Team Update Successfully!!');
    }
}
