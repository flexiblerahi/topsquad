<?php

namespace App\Http\Controllers\Api;

use App\Models\Contest;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContestResource;
use App\Models\ContestRank;
use App\Models\ContestUser;
use App\Models\MatchTeam;
use Carbon\Carbon;

class ContestController extends Controller
{
    public function index($matchId)
    {
        $contests = Contest::where('match_id', $matchId)->latest()->get();
        $contests->load('ranks');
        $contests->load('contestUser');
        return $this->apiResponseResourceCollection(200, 'all Contests', ContestResource::collection($contests));
    }

    public function joinContest(Contest $contest)
    {
        $contest->load('match');
        if($contest->match->status == MatchTeam::LIVE) return $this->apiResponse(404, 'Sorry!!! This match is in live.');
        $contests = ContestUser::where('contest_id', $contest->id)->get();
        if($contest->count <= count($contests)) return $this->apiResponse(404, 'This contest already fill up');
        $contest_user = $contests->where('user_id', auth()->id());
        if(count($contest_user) > 0) return $this->apiResponse(404, 'Already Joined in contest');
        if(auth()->user()->coin <= $contest->entry_fee) return $this->apiResponse(404, 'Insufficient Balance!!!');
        auth()->user()->decrement('coin', $contest->entry_fee);
        auth()->user()->save();

        ContestUser::create(array(
            'contest_id'=> $contest->id,
            'match_id' => $contest->match_id,
            'user_id' => auth()->user()->id
        ));
        return $this->apiResponse(201, 'Join Contest Successfully');
    }
}
