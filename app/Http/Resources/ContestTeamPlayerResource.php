<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContestTeamPlayerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'username'=>auth()->user()->username,
            'userimage' =>setImage(auth()->user()->image, 'user'),
            'id' => $this->id,
            'contest_id' => $this->contest_id,
            'vice_captain' => $this->vice_captain,
            'captain' => $this->captain,
            'totalpoints' => $this->totalPoint,
            'players' => MyContestPlayerResource::collection($this->players),
        ];
    }
}
