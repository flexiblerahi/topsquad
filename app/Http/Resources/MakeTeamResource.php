<?php

namespace App\Http\Resources;

use App\Models\Player;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class MakeTeamResource extends JsonResource
{
    public function toArray($request)
    {
        $totalPlayers = $this->whenLoaded('teamoneOne')->players->merge( $this->whenLoaded('teamtwoOne')->players);
        $batsmans = array();
        $bowlers= array();
        $wicketkeepers = array();
        $allrounders = array();
        foreach ($totalPlayers as $player) {
            $shortname =($this->teamoneOne->id == $player->pivot->team_id) ? $this->teamoneOne->shortname : $this->teamtwoOne->shortname;
            if($player->position == Player::BATSMAN) {

                $batsmans[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => false,
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            } else if ($player->position == Player::BOWLER) {
                $bowlers[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => false,
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            } else if ($player->position == Player::WICKETKEEPER) {
                $wicketkeepers[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => false,
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            } else {
                $allrounders[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => false,
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            }
        }
        return [
            'allrounders' => $allrounders,
            'bowlers' => $bowlers,
            'batsmans' => $batsmans,
            'wicketkeepers' => $wicketkeepers,
        ];
    }
}
