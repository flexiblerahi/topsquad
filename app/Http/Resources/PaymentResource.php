<?php

namespace App\Http\Resources;

use App\Models\Payment;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    public function toArray($request)
    {
        $approve_at = $this->approve_at == null ? 'Not Approve Yet' : $this->approve_at->diffForHumans();
        $deleted_at = $this->deleted_at == null ? '' : 'Deleted By Admin at '.$this->approve_at->diffForHumans();
        if ($this->status == Payment::PENDING) $status = 'Request Pending';
        else if ($this->status == Payment::APPROVE) $status = 'Request Approved';
        else $status = 'Request Cancel';
        return [
            'transaction_id' => $this->transaction_id,
            'amount' => $this->amount,
            'method' => $this->method,
            'created_at' => dateFormat($this->created_at),
            'approve_at' => $approve_at,
            'status' => $status,
            'deleted_at' => $deleted_at,
        ];
    }
}
