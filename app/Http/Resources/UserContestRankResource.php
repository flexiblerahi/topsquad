<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Resources\Json\JsonResource;

class UserContestRankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $dt = new DateTime();

        $userContestRanks = $this->whenLoaded('userContestRanks');

        $userRank = $userContestRanks->where('user_id', auth()->id())->first();
        return [
            "contest_name" => $this->name,
            "contest_shortname" => $this->shortname,
            "contest_update_time" =>  date("d M, Y h:i a", strtotime($this->updated_at)),
            "score" => [
                "team_1" => [
                    "name" => $this->match->teamoneOne->name,
                    'image' => setImage($this->match->teamoneOne->image),
                    "run" => $this->match->team_one_run, //$this->match->teamOne->first()->run,
                    "wicket" => $this->match->team_one_wicket, //this->match->teamOne->first()->wicket,
                    "over" => $this->match->team_one_over, //this->match->teamOne->first()->over,
                ],
                "team_2" => [
                    "name" => $this->match->teamtwoOne->name,
                    'image' => setImage($this->match->teamtwoOne->image),
                    "run" => $this->match->team_two_run, //$this->match->teamOne->first()->run,
                    "wicket" => $this->match->team_two_wicket, //this->match->teamOne->first()->wicket,
                    "over" => $this->match->team_two_over, //this->match->teamOne->first()->over,
                ],
            ],
            "user_rank" => $userRank == null ? [
                "name" => auth()->user()->username,
                "image" => setImage(auth()->user()->image, 'user'),
                "rank" =>  0,
                "point" => 0,
                ]:[
                "name" => auth()->user()->username,
                "image" => setImage(auth()->user()->image, 'user'),
                "rank" =>  $userRank->position,
                "point" => $userRank->totalpoints,
            ],
            "all_ranks" => UserRanksResource::collection($userContestRanks),
        ];
    }
}


// $today = date("F j, Y, g:i a");               // March 10, 2001, 5:16 pm
// $today = date("m.d.y");                       // 03.10.01
// $today = date("j, n, Y");                     // 10, 3, 2001
// $today = date("Ymd");                         // 20010310
// $today = date('h-i-s, j-m-y, it is w Day');   // 05-16-18, 10-03-01, 1631 1618 6 Satpm01
// $today = date('\i\t \i\s \t\h\e jS \d\a\y.'); // it is the 10th day.
// $today = date("D M j G:i:s T Y");             // Sat Mar 10 17:16:18 MST 2001
// $today = date('H:m:s \m \i\s\ \m\o\n\t\h');   // 17:03:18 m is month
// $today = date("H:i:s");                       // 17:16:18
// $today = date("Y-m-d H:i:s");                 // 2001-03-10 17:16:18 (the MySQL DATETIME format)
