<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $ranks = $this->whenLoaded('ranks');
        $totalPrize = 0;
        $prizeType= '';
        $is_join = false;
        $contestUsers = $this->whenLoaded('contestUser');
        foreach ($ranks as $rank) {
            $totalPrize = $rank->amount + $totalPrize;
            $prizeType = $rank->amount_type;
        }

        foreach ($contestUsers as $contest_user) {
            if($contest_user->user_id == auth()->id()) $is_join = true;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'shortname' => $this->shortname,
            'entry_fee' => $this->entry_fee,
            'entry_fee_type' => $this->entry_fee_type,
            'team' => $contestUsers->count(),
            'can_join_team_number' => $this->count,
            'totalPrize' => $totalPrize,
            'prizeType' => $prizeType,
            'is_join' => $is_join,
            'ranks' => RankResource::collection($this->whenLoaded('ranks'))

        ];
    }
}
