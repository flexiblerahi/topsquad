<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyContestTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // dd($this->contest->ranks);

        $ranks = $this->contest->ranks;
        $totalPrize = 0;
        $prizeType= '';
        foreach ($ranks as $rank) {
            $totalPrize = $rank->amount + $totalPrize;
            $prizeType = $rank->amount_type;
        }
        return [
            'id' => $this->contest->id,
            'name' => $this->contest->name,
            'shortname' => $this->contest->shortname,
            'entry_fee' => $this->contest->entry_fee,
            'entry_fee_type' => $this->contest->entry_fee_type,
            'team' => $this->contest->contestUser->count(),
            'can_join_team_number' => $this->contest->count,
            'totalPrize' => $totalPrize,
            'prizeType' => $prizeType,
            'ranks' => RankResource::collection($this->contest->ranks)
        ];
    }
}
