<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $matches =array();
        foreach ($this->whenLoaded('contests') as $contest) {
            if(!in_array($contest->match_id, $matches)) array_push($matches, $contest->match_id);
        }
        return [
            'username' => $this->username,
            'email' => $this->email,
            'phone' => $this->phone,
            'image' => setImage($this->image, 'user'),
            'coin' => $this->coin,
            'point' => $this->point,
            'money' => $this->money,
            'country' => $this->country,
            'gender' => $this->gender,
            'matchCount' => count($matches),
            'contestCount' => $this->whenLoaded('contests')->count(),
            'rankCount' => $this->whenLoaded('ranks')->count(),

        ];
    }
}
