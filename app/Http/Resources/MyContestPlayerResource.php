<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyContestPlayerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'playerimg' => setImage($this->image, 'user'),
            'position' => playerPosition($this->position),
            'point' => $this->point,
            'is_vice_captain' => $this->vice_captain,
            'is_captain' => $this->captain,
            'is_announce' => $this->is_announce == 0 ? false : true
        ];
    }
}
