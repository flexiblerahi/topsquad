<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MatchTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        // dd($this);
        return [
            'id' => $this->id,
            'match_started_at' => setDateTime($this->match_started_at),
            'league' => LeagueResource::collection($this->whenLoaded('league')),
            'teamone' => LeagueResource::collection($this->whenLoaded('teamone')),
            'teamtwo' => LeagueResource::collection($this->whenLoaded('teamtwo')),
        ];
    }
}
