<?php

namespace App\Http\Resources;

use App\Models\Player;
use Illuminate\Http\Resources\Json\JsonResource;

class ContestMakeTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $matchTeam = $this->whenLoaded('matchTeam');
        $totalPlayers = $matchTeam->teamoneOne->players->merge($matchTeam->teamtwoOne->players);
        $batsmans = array();
        $bowlers = array();
        $wicketkeepers = array();
        $allrounders = array();
        foreach ($totalPlayers as $player) {
            $shortname = ($matchTeam->teamoneOne->id == $player->pivot->team_id) ? $matchTeam->teamoneOne->shortname : $matchTeam->teamtwoOne->shortname;
            if ($player->position == Player::BATSMAN) {

                $batsmans[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => in_array($player->id, json_decode($this->players)),
                    'is_announced' => $player->is_announce == 1 ? true : false,
                );
            } else if ($player->position == Player::BOWLER) {
                $bowlers[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => in_array($player->id, json_decode($this->players)),
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            } else if ($player->position == Player::WICKETKEEPER) {
                $wicketkeepers[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => in_array($player->id, json_decode($this->players)),
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            } else {
                $allrounders[] = array(
                    'id' => $player->id,
                    'name' => $player->name,
                    'credit' => $player->credit,
                    'position' => playerPosition($player->position),
                    'image' => setImage($player->image, 'user'),
                    'team_id' => $player->pivot->team_id,
                    'player_id' => $player->pivot->player_id,
                    'team_shortname' => $shortname,
                    'is_join' => in_array($player->id, json_decode($this->players)),
                    'is_announced' => $player->is_announce == 1 ? true : false
                );
            }
        }
        return [
            "id" => $this->id,
            "name" => $this->name,
            "vice_captain" => $this->vice_captain,
            "captain" => $this->captain,
            'allrounders' => $allrounders,
            'bowlers' => $bowlers,
            'batsmans' => $batsmans,
            'wicketkeepers' => $wicketkeepers,
        ];
    }
}
