<?php

namespace App\Http\Resources;

use App\Models\Withdraw;
use Illuminate\Http\Resources\Json\JsonResource;

class WithdrawResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status = '';
        if($this->status == Withdraw::PENDING) $status = "Withdraw request pending";
        elseif($this->status == Withdraw::APPROVE) $status = "Withdraw successfully send";
        else $status = "Withdraw cancel";
        return [
            'status' => $status,
            'mobile' => $this->mobile,
            'method' => $this->method,
            'amount' => $this->amount,
            'created_at' => setDateTime($this->created_at),
            'approve_at' => $this->approve_at == null? 'not approve yet' : setDateTime($this->approve_at)
        ];
    }
}
