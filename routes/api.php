<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ContestController;
use App\Http\Controllers\Api\TeamController;
use App\Http\Controllers\Api\LeagueController;
use App\Http\Controllers\Api\SliderController;
use App\Http\Controllers\Api\PlayerController;
use App\Http\Controllers\Api\MatchTeamController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\ContestTeamController;
use App\Http\Controllers\Api\ContestUserController;
use App\Http\Controllers\Api\ForgetPasswordController;
use App\Http\Controllers\Api\PackageController;
use App\Http\Controllers\Api\PromoCodeController;
use App\Http\Controllers\Api\UserContestRankController;
use App\Http\Controllers\Api\WithdrawController;

Route::middleware('apiRules')->group(function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post("login", [AuthController::class, 'login']);
    Route::post("forget-password/email", [ForgetPasswordController::class, 'sendForgetPassword']);
    Route::post("forget-password/code", [ForgetPasswordController::class, 'codeVerify']);
    Route::post("forget-password/", [ForgetPasswordController::class, 'forgetPassword']);

    Route::middleware('auth:sanctum')->group(function () {
        //user profile api
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::get('/profile/view', [AuthController::class, 'show']);
        Route::put('/profile/update', [AuthController::class, 'changeProfile']);
        Route::put('/change-password', [AuthController::class, 'changePassword']);
        Route::post('/change-profile-picture', [AuthController::class, 'changeProfilepicture']);
        //others
        Route::get('/slider', [SliderController::class, 'index']);

        Route::apiResource('team', TeamController::class)->only(['index', 'show']);
        Route::apiResource('league', LeagueController::class)->only(['index', 'show']);
        Route::apiResource('match', MatchTeamController::class)->only(['index', 'show']);
        Route::apiResource('package', PackageController::class);

        Route::apiResource('promo', PromoCodeController::class);


        Route::get('/player', [PlayerController::class, 'index']);

        Route::post('/payment/profile', [PaymentController::class, 'profilePay']);
        Route::post('/payment/method', [PaymentController::class, 'manualPay']);
        Route::get('/payment/history', [PaymentController::class, 'show']);

        Route::get('/match/contest/{id}', [ContestController::class, 'index']);
        Route::post('/join/contest/{contest}', [ContestController::class, 'joinContest']);

        Route::get('create/team/{id}', [MatchTeamController::class, 'createTeam']);

        Route::post('store/contest/team/{id}', [ContestTeamController::class, 'store']);
        Route::prefix('/contest/team/list')->group(function () {
            Route::get('/', [ContestTeamController::class, 'index']);
            Route::get('/{id}', [ContestTeamController::class, 'show']); // contest er id
            Route::get('/edit/{id}', [ContestTeamController::class, 'edit']); // contest er id
            Route::put('/update/{contest_team}', [ContestTeamController::class, 'update']);
        });

        Route::get('all/my-contest', [ContestUserController::class, 'index']);
        Route::get('my-contest/under/match/{id}', [ContestUserController::class, 'myContestList']); // match id

        Route::post('withdraw', [WithdrawController::class, 'store']);
        Route::get('withdraw/history', [WithdrawController::class, 'index']);

        // Route
        Route::get('/contest/score/{id}', [UserContestRankController::class, 'index']);
    });
});
