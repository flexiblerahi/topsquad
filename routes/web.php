<?php

use App\Http\Controllers\Admin\AdminPayrollController;
use App\Http\Controllers\Admin\LeagueController;
use App\Http\Controllers\Admin\MatchTeamController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RegisterOfferController;
use App\Http\Controllers\Admin\ContestController;
use App\Http\Controllers\Admin\ContestRankController;
use App\Http\Controllers\Admin\ContestTeamController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\PlayerController;
use App\Http\Controllers\Admin\PromocodeController;
use App\Http\Controllers\Admin\PushNotificationController;
use App\Http\Controllers\Admin\WithdrawController;
use App\Http\Controllers\Website\HomeController;
use App\Models\MatchTeam;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::get('command-route', function () {
    Artisan::call('cache:clear');
    Artisan::call('migrate:fresh --seed');
});

Route::middleware('web')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
});

Route::prefix('dashboard')->middleware(['auth', 'verified'])->group(function () {
    Route::view('/', 'dashboard.dashboard')->name('dashboard');
    //Profile Setting
    Route::resource('user', UserController::class);
    Route::get('edit-profile',  [UserController::class, 'editProfile'])->name('edit.profile');
    Route::put('edit-update-profile', [UserController::class, 'updateProfile'])->name('edit.profile.update');
    Route::get('change_password', [UserController::class, 'change_password'])->name('change_password');

    Route::prefix('settings')->group(function () {
        Route::get('/company_settings', [SettingController::class, 'editCompanySetting'])->name('company.edit');
        Route::post('/company_setting', [SettingController::class, 'updateCompanySetting'])->name('company.update');
    });
    // Role Permission
    Route::resource('developer/permission', PermissionController::class)->only('index', 'store');
    Route::get('role/assign', [RoleController::class, 'roleAssign'])->name('role.assign');
    Route::post('role/assign', [RoleController::class, 'storeAssign'])->name('store.assign');
    Route::resource('role', RoleController::class);
    Route::resource('slider', SliderController::class);
    Route::resource('team', TeamController::class);
    Route::resource('league', LeagueController::class);
    Route::resource('match_team', MatchTeamController::class);
    Route::resource('contest', ContestController::class);
    Route::resource('register-offer', RegisterOfferController::class);
    Route::resource('package', PackageController::class);
    Route::resource('payment', PaymentController::class);
    Route::resource('player', PlayerController::class);
    Route::resource('withdraw', WithdrawController::class);
    Route::resource('admin_payroll', AdminPayrollController::class);
    // Route::resource('', PromocodeController::class);
    Route::resource('promocode', PromocodeController::class);


    Route::prefix('payment')->group(function () {
        Route::get('/cancel/{payment}', [PaymentController::class, 'cancel'])->name('payment.cancel');
    });
    Route::get('cancel/payment', [PaymentController::class, 'cancelHistory'])->name('payment.cancel.list');
    Route::get('approve/payment', [PaymentController::class, 'approve'])->name('payment.approve');

    Route::get('contest-rank/{id}', [ContestRankController::class, 'index'])->name('contest.rank.list'); //parameter id is contest id
    Route::post('contest-rank/store/{id}', [ContestRankController::class, 'store'])->name('contest.rank.store'); //parameter id is contest id
    Route::put('contest-rank/update/{contest_rank}', [ContestRankController::class, 'update'])->name('contest.rank.update'); //parameter id is contest id
    Route::delete('rankcontest/delete/{contest_rank}', [ContestRankController::class, 'destroy'])->name('contest.rank.delete'); //parameter id is contest id

    Route::get('player/create/{id}/{position}', [PlayerController::class, 'create'])->name('player.team.create');
    Route::post('player/team/store/{id}', [PlayerController::class, 'store'])->name('player.team.store');

    Route::get('match/live', [MatchTeamController::class, 'live'])->name('match.live');
    Route::get('match/completed', [MatchTeamController::class, 'completed'])->name('match.completed');
    Route::get('match/history', [MatchTeamController::class, 'history'])->name('match.history');
    Route::get('match/announce/player/list/{match_team}', [MatchTeamController::class, 'announcePlayers'])->name('announce.players');
    Route::get('live/score/{match_team}', [MatchTeamController::class, 'livescore'])->name('live.score');
    Route::put('live/score/{match_team}', [MatchTeamController::class, 'livescoreStore'])->name('live.score.match_team.update');

    Route::get('contest/team/complete/{id}', [ContestTeamController::class, 'complete'])->name('contest.team.complete');
    Route::get('approved/withdraw', [WithdrawController::class, 'approve'])->name('withdraw.approve');
    Route::put('cancel/withdraw/{withdraw}', [WithdrawController::class, 'cancel'])->name('cancel.withdraw');
    Route::get('contest-history', [ContestController::class, 'history'])->name('contest.history');
    Route::get('running/contest', [ContestController::class, 'running'])->name('contest.running');
    Route::get('contest/users/{id}', [ContestTeamController::class, 'users'])->name('contest.users');
    Route::post('contest/players/update', [PlayerController::class, 'updateplayers'])->name('points.update');

    Route::get('/send-notification/index', [PushNotificationController::class, 'index'])->name('notification');
    Route::get('/send-notification/create', [PushNotificationController::class, 'create'])->name('notification.create');
    Route::delete('/send-notification/delete/{notification}', [PushNotificationController::class, 'destroy'])->name('notification.destroy');


    Route::post('/send-notification', [PushNotificationController::class, 'sendNotification'])->name('send.notification');
    //coupons
});
