@component('mail::message')

{{-- @php
    dd($data['code']);
@endphp --}}
<div style="display: flex;justify-content: center;margin-top:20px;">
  Your verification code is {{$data['code']}}
</div>

{{-- <p style="text-align: center">Added a new vehicle for preparation at {{date('H:i a', strtotime($data['vehicle']->prepare_date))}} on {{date('m/d/Y', strtotime($data['vehicle']->prepare_date))}}<p> --}}


@endcomponent