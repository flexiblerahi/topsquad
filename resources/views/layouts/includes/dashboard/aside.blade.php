<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <!--begin::Brand-->
    <div class="brand flex-column-auto" id="kt_brand">
        <!--begin::Logo-->
        <a href="{{ url('/') }}" class="brand-logo" style="height: 100%; max-height: 100%;">
            @if ($company->logo)
                <img alt="Logo" src="{{ setImage($company->logo) }}" class="py-2" style="height: 100%; width: auto" />
            @else
                <h4 class="logo-alt mb-0 d-flex align-items-center">{{ $company->name }}</h4>
            @endif
        </a>
        <!--end::Logo-->
        <!--begin::Toggle-->
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
            <span class="svg-icon svg-icon svg-icon-xl">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path
                            d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                            fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                        <path
                            d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                            fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
        </button>
        <!--end::Toolbar-->
    </div>
    <!--end::Brand-->
    <!--begin::Aside Menu-->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <!--begin::Menu Container-->
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <!--begin::Menu Nav-->
            <ul class="menu-nav">
                <li class="menu-item {{ request()->is('dashboard') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('dashboard') }}" class="menu-link">
                        <span class="svg-icon menu-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path
                                        d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z"
                                        fill="#000000" fill-rule="nonzero" />
                                    <path
                                        d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z"
                                        fill="#000000" opacity="0.3" />
                                </g>
                            </svg>
                        </span>
                        <span class="menu-text">Dashboard</span>
                    </a>
                </li>

                @canany(['user.access', 'user.create'])
                <li class="menu-item {{ request()->is('dashboard/user*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">User</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">

                      @can('user.access')
                      <li class="menu-item {{ request()->is('dashboard/user') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('user.index') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">User</span>
                        </a>
                      </li>

                      @endcan

                      @can('user.create')
                        <li class="menu-item {{ request()->is('dashboard/user/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('user.create') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Create</span>
                          </a>
                        </li>
                        @endcan

                    </ul>
                  </div>
                </li>
                @endcanany
                @canany(['slider.access', 'slider.create'])
                <li class="menu-item {{ request()->is('dashboard/slider*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Slider</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('slider.access')
                        <li class="menu-item {{ request()->is('dashboard/slider') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('slider.index') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Slider</span>
                          </a>
                        </li>
                        @endcan
                        @can('slider.create')
                        <li class="menu-item {{ request()->is('dashboard/slider/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('slider.create') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Create</span>
                          </a>
                        </li>
                        @endcan

                    </ul>
                  </div>
                </li>
                @endcanany

                @canany(['league.access', 'league.create'])
                <li class="menu-item {{ request()->is('dashboard/league*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Leauge</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('league.access')
                      <li class="menu-item {{ request()->is('dashboard/league') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('league.index') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Leauge</span>
                        </a>
                      </li>
                      @endcan

                      @can('league.create')
                      <li class="menu-item {{ request()->is('dashboard/league/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('league.create') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Create</span>
                        </a>
                      </li>
                      @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                @canany(['payment.access', 'payment.approve', 'payment.cancel.list'])
                <li class="menu-item {{ request()->routeIs('payment*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Payment</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">

                      @can('payment.access')
                      <li class="menu-item {{ request()->routeIs('payment.index') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('payment.index') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Payment List</span>
                        </a>
                      </li>
                      @endcan

                      @can('payment.approve')
                        <li class="menu-item {{ request()->routeIs('payment.approve') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('payment.approve') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Approve History</span>
                          </a>
                        </li>
                        <li class="menu-item {{ request()->routeIs('payment.cancel.list') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('payment.cancel.list') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Cancel History</span>
                          </a>
                        </li>
                        @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                @canany(['match_team.access', 'match_team.create', 'match.live', 'match.completed' , 'match.completed' , 'match.history'])
                <li class="menu-item {{ request()->routeIs('match_team*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Tournament</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('match_team.create')
                      <li class="menu-item {{ request()->routeIs('match_team.create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('match_team.create') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Create Match</span>
                        </a>
                      </li>
                      @endcan
                      @can('match_team.access')
                        <li class="menu-item {{ request()->routeIs('match_team.index') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('match_team.index') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Matches</span>
                          </a>
                        </li>
                        @endcan
                        @can('match.live')
                        <li class="menu-item {{ request()->routeIs('match.live') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('match.live') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Live</span>
                          </a>
                        </li>
                        @endcan
                        @can('match.completed')
                        <li class="menu-item {{ request()->routeIs('match.completed') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('match.completed') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Completed</span>
                          </a>
                        </li>
                        @endcan
                    </ul>
                  </div>
                </li>
                @endcanany

                @canany(['package.access', 'package.create'])
                {{-- end  --}}
                <li class="menu-item {{ request()->routeIs('package*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Package</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('package.index')
                        <li class="menu-item {{ request()->routeIs('package.index') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('package.index') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Packages</span>
                          </a>
                        </li>
                        @endcan
                        @can('package.create')
                        <li class="menu-item {{ request()->routeIs('package.create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('package.create') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Create Package</span>
                          </a>
                        </li>
                        @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                @canany(['withdraw.access', 'withdraw.create'])
                <li class="menu-item {{ request()->routeIs('withdraw*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Withdraw</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('withdraw.access')
                      <li class="menu-item {{ request()->routeIs('withdraw.index') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('withdraw.index') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Pending requests</span>
                        </a>
                      </li>
                      @endcan
                      @can('withdraw.approve')
                        <li class="menu-item {{ request()->routeIs('withdraw.approve') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('withdraw.approve') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Approved/cancel</span>
                          </a>
                        </li>
                        @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                @canany(['team.access', 'team.create'])
                <li class="menu-item {{ request()->is('dashboard/team*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Team</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('team.access')
                        <li class="menu-item {{ request()->is('dashboard/team') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('team.index') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Team</span>
                          </a>
                        </li>
                        @endcan
                        @can('team.create')
                        <li class="menu-item {{ request()->is('dashboard/team/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('team.create') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Create</span>
                          </a>
                        </li>
                        @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                @canany(['contest.access', 'contest.create' ,'contest.history' , 'contest.running'])
                <li class="menu-item {{ request()->is('dashboard/team*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Notification</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">

                        <li class="menu-item {{ request()->is('dashboard/send-notification') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('notification') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Index</span>
                          </a>
                        </li>
                        <li class="menu-item {{ request()->is('dashboard/send-notification/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('notification.create') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Create</span>
                          </a>
                        </li>
                    </ul>
                  </div>
                </li>

                <li class="menu-item {{ request()->is('dashboard/promocode*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Promo Code</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">

                        <li class="menu-item {{ request()->is('dashboard/promocode') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('promocode.index') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Index</span>
                          </a>
                        </li>
                        @can('tepromocodeam.create')
                        <li class="menu-item {{ request()->is('dashboard/promocode/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                          <a href="{{ route('promocode.create') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                              <span></span>
                            </i>
                            <span class="menu-text">Create</span>
                          </a>
                        </li>
                        @endcan
                    </ul>
                  </div>
                </li>
                
                <li class="menu-item {{ request()->is('dashboard/contest*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Contest</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('contest.create')
                      <li class="menu-item {{ request()->is('dashboard/contest/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('contest.create') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Create</span>
                        </a>
                      </li>
                      @endcan
                      @can('contest.access')
                      <li class="menu-item {{ request()->routeIs('contest.index') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('contest.index') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Upcoming contests</span>
                        </a>
                      </li>
                      @endcan
                      @can('contest.history')
                      <li class="menu-item {{ request()->routeIs('contest.history') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('contest.history') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">History contests</span>
                        </a>
                      </li>
                      @endcan
                      @can('contest.running')
                      <li class="menu-item {{ request()->routeIs('contest.running') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('contest.running') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Running contests</span>
                        </a>
                      </li>
                      @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                {{-- START  --}}

                @canany(['admin_payroll.access', 'admin_payroll.create.create'])
                <li class="menu-item {{ request()->routeIs('admin_payroll*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
                  <a href="javascript:" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                          <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                        </g>
                      </svg>
                    </span>
                    <span class="menu-text">Admin Payroll</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                      @can('admin_payroll.create')
                      <li class="menu-item {{ request()->routeIs('admin_payroll.create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('admin_payroll.create') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">Create</span>
                        </a>
                      </li>
                      @endcan
                      @can('admin_payroll.access')
                      <li class="menu-item {{ request()->routeIs('admin_payroll.index') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('admin_payroll.index') }}" class="menu-link">
                          <i class="menu-bullet menu-bullet-line">
                            <span></span>
                          </i>
                          <span class="menu-text">all Payrolls</span>
                        </a>
                      </li>
                      @endcan
                    </ul>
                  </div>
                </li>
                @endcanany
                {{-- END  --}}
                @if (auth()->user()->hasRole('User'))
                    @include('layouts.includes.dashboard.menu.user-role-specific-menu')
                @endif

                @include('layouts.includes.dashboard.menu.global-menu')

            </ul>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>
