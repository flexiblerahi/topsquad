@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Package Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST" action="{{ route('package.store') }}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text"
                            class="form-control" name="name" id="name" aria-describedby="nameId" placeholder="Enter Package Name">
                          <small id="nameId" class="form-text text-muted">package name</small>
                        </div>
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-around">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <label for="coin">Coin</label>
                          <input type="number"
                            class="form-control" name="coin" id="coin" aria-describedby="coinId" placeholder="Enter Coin">
                          <small id="coinId" class="form-text text-muted">Coin for contest</small>
                        </div>
                        @error('coin')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label for="money">Money</label>
                        <input type="number"
                          class="form-control" name="money" id="money" aria-describedby="moneyId" placeholder="Enter money">
                        <small id="moneyId" class="form-text text-muted">Buy coin with money for contest</small>
                      </div>
                      @error('coin')
                          <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  <div class="col-lg-11  offset-lg-1 d-flex">
                    <label class="col-form-label mr-2" for="status">Status</label>
                      <input type="hidden" checked value="0" name="status" />
                      <span class="switch switch-icon">
                          <label>
                              <input type="checkbox" {{ old('status', 1) ? 'checked' : null }} value="1"
                                  name="status" />
                              <span></span>
                          </label>
                      </span>
                      @error('status')
                          <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                </div>
            </form>
        </div>
    </div>
@endsection
