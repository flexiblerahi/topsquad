@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Match Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Update Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST" action="{{ route('live.score.match_team.update', $match_team->id) }}">
                @csrf
                @method('PUT')
                <h4>{{ $match_team->teamoneOne->name }}</h4>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label for="team_one_run">Run</label>
                      <input type="number"
                        class="form-control" name="team_one_run" id="team_one_run" aria-describedby="team_one_runId" value="{{ $match_team->team_one_run }}">
                      <small id="team_one_runId" class="form-text text-muted">{{ $match_team->teamoneOne->name }} team run</small>
                      @error('team_one_run')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="team_one_wicket">Wicket</label>
                      <input type="number"
                        class="form-control" name="team_one_wicket" id="team_one_wicket" aria-describedby="team_one_wicketId" value="{{ $match_team->team_one_wicket }}">
                      <small id="team_one_wicketId" class="form-text text-muted">{{ $match_team->teamoneOne->name }} team wicket</small>
                    </div>
                    @error('team_one_wicket')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label for="team_one_over">Over</label>
                      <input type="text"
                        class="form-control" name="team_one_over" id="team_one_over" aria-describedby="team_one_overId" value="{{ $match_team->team_one_over }}">
                      <small id="team_one_overId" class="form-text text-muted">{{ $match_team->teamoneOne->name }} team Over</small>
                    </div>
                    @error('team_one_over')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                  </div>
                </div>
                <hr>
                <h4 class="mt-3">{{ $match_team->teamtwoOne->name }}</h4>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label for="team_two_run">Run</label>
                      <input type="number"
                        class="form-control" name="team_two_run" id="team_two_run" aria-describedby="team_two_runId" value="{{ $match_team->team_two_run }}">
                      <small id="team_two_runId" class="form-text text-muted">{{ $match_team->teamtwoOne->name }} team run</small>
                    </div>
                    @error('team_two_run')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label for="team_two_wicket">Wicket</label>
                      <input type="number"
                        class="form-control" name="team_two_wicket" id="team_two_wicket" aria-describedby="team_two_wicketId" value="{{ $match_team->team_two_wicket }}">
                      <small id="team_two_wicketId" class="form-text text-muted">{{ $match_team->teamtwoOne->name }} team wicket</small>
                    </div>
                    @error('team_two_wicket')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label for="team_two_over">Over</label>
                      <input type="text"
                        class="form-control" name="team_two_over" id="team_two_over" aria-describedby="team_two_overId" value="{{ $match_team->team_two_over }}">
                      <small id="team_two_overId" class="form-text text-muted">{{ $match_team->teamtwoOne->name }} team Over</small>
                    </div>
                    @error('team_two_over')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                  </div>
                </div>
            </form>
        </div>
    </div>
@endsection

