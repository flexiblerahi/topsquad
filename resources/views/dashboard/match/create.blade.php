@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Match Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST" action="{{ route('match_team.store') }}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label for="leagueId">Select League</label>
                            <select class="form-control" required id="leagueId" name="league_id">
                                <option value="">Choose League</option>

                            </select>
                            @error('league_id')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="selectTeam1">Select Team</label>
                            <select class="form-control" id="selectTeam1" required name="team_one_id">
                                <option value="">Choose Team</option>
                            </select>
                            @error('team_one_id')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-2 text-center">
                        <div class="challenge">
                            <h2 class="mt-10">Vs</h2>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="selectTeam2">Select Team</label>
                            <select class="form-control" id="selectTeam2" required name="team_two_id">
                                <option value="">Choose Team</option>
                            </select>
                            @error('team_two_id')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label for="matchtime">Match time</label>
                            <input type="datetime-local" class="form-control" name="match_started_at" id="matchtime"
                                aria-describedby="matchtimeId" placeholder="Enter match date">
                            <small id="matchtimeId" class="form-text text-muted">Match Start Time</small>
                            @error('match_started_at')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                  <div class="col-lg-10">
                    <label class="mr-5 mb-5" for="status">Status</label>
                    <div class="check d-flex">
                      <div class="form-check mr-5">
                        <input class="form-check-input" type="radio" name="status" id="status1" value="{{ App\Models\MatchTeam::UPCOMING }}" checked>
                        <label class="form-check-label" for="status1">
                          Upcoming
                        </label>
                      </div>
                      <div class="form-check mr-5">
                        <input class="form-check-input" type="radio" name="status" id="status2" value="{{ App\Models\MatchTeam::LIVE }}">
                        <label class="form-check-label" for="status2">
                          Live
                        </label>
                      </div>
                      <div class="form-check mr-5 disabled">
                        <input class="form-check-input" type="radio" name="status" id="status3" value="{{ App\Models\MatchTeam::COMPLETED }}">
                        <label class="form-check-label" for="status3">
                          Completed
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/dashboard/js/pages/crud/file-upload/image-input.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        let avatar5 = new KTImageInput('logo');
        let avatar6 = new KTImageInput('footerLogo');
    </script>
    <script>
        let leagues = @json($leagues);
        let teams = '';
        $(document).ready(function() {
            leagues.forEach(league => {
                $('#leagueId').append($('<option>', {
                    value: league.id,
                    text: league.name
                }));
            });
            $(document).on('change', '#leagueId', function() {
                let leagueId = $(this).val();
                var length = $('#selectTeam1').children('option').length;
                if($('#selectTeam1').children('option').length>1) {
                    $('#selectTeam1 option:not(:first)').remove();
                    $('#selectTeam2 option:not(:first)').remove();
                }
                teams = leagues.find((league) => league.id == leagueId);
                teams.teams.forEach(team => {
                    $('#selectTeam1').append($('<option>', {
                        value: team.id,
                        text: team.name
                    }));
                    $('#selectTeam2').append($('<option>', {
                        value: team.id,
                        text: team.name
                    }));
                });
            });
            $(document).on('change', '#selectTeam1', function() {
                let teamId = $(this).val();
                $('#selectTeam2')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">Choose Team</option>');
                let filterteam = teams.teams.filter((team) => team.id != teamId);
                filterteam.forEach(team => {
                    $('#selectTeam2').append($('<option>', {
                        value: team.id,
                        text: team.name
                    }));
                });
            });
        })
    </script>

@endpush
