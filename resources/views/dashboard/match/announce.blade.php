@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-body">
                  <h5>{{ $match_team->teamoneOne->name }} Players</h5>
                  <form action="{{ route('points.update') }}" method="post">
                    @csrf
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>Players</th>
                                <th>Point</th>
                                <th><button type="submit" class="btn btn-primary">Submit</button></th>
                            </tr>
                        </thead>
                        <tbody>

                              @foreach ($match_team->teamoneOne->players as $player)
                                @if($player->is_announce == 1)
                                  <tr>
                                    <th>{{ $player->name }}</th>
                                    <th>
                                      <div class="form-group">
                                        <input type="text"
                                          class="form-control" name="point-{{ $player->id }}" value={{ $player->point }} id="point" aria-describedby="helpId" >
                                      </div>
                                    </th>
                                  </tr>
                                @endif
                              @endforeach

                        </tbody>
                    </table>
                  </form>
                  <h5>{{ $match_team->teamtwoOne->name }} Players</h5>
                  <form action="{{ route('points.update') }}" method="post">
                    @csrf
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>Players</th>
                                <th>Point</th>
                                <th><button type="submit" class="btn btn-primary">Submit</button></th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($match_team->teamtwoOne->players as $player)
                              @if($player->is_announce == 1)
                                <tr>
                                  <th>{{ $player->name }}</th>
                                  <th>
                                    <div class="form-group">
                                      <input type="text"
                                        class="form-control" name="point-{{ $player->id }}" value={{ $player->point }} id="point" aria-describedby="helpId" >
                                    </div>
                                  </th>
                                </tr>
                              @endif
                            @endforeach

                        </tbody>
                    </table>
                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection

