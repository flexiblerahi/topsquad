@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Create Notification
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" enctype="multipart/form-data" method="POST" action="{{ route('send.notification') }}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-6">

                        <div class="form-row">
                          
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="mobile1">{{ __('Title') }}</label>
                                    <input name="title" id="title" value="{{ old('title')  }}" placeholder="title" type="text"
                                        class="form-control form-control-solid @error('title') is-invalid @enderror">
                                    @error('title')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                  <label for="body">{{ __('Body') }}</label>
                                  <input name="body" id="body" value="{{ old('body')  }}" placeholder="body" type="text"
                                      class="form-control form-control-solid @error('body') is-invalid @enderror">
                                  @error('phone')
                                      <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                  @enderror
                              </div>
                          </div>
                      
                        
                        </div>





                    </div>
                </div>



            </form>
        </div>
    </div>
@endsection
