@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Slider Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" enctype="multipart/form-data" method="POST"
                action="{{ route('league.store') }}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="name">{{ __('Leauge Name') }} <span class="text-danger">*</span></label>
                            <input name="name" id="name" value="{{ old('name') }}"
                                class="form-control form-control-solid @error('name') is-invalid @enderror" type="text"
                                placeholder="Leauge Name">
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image" class="d-block">{{ __('League Logo') }}</label>
                                    <div class="image-input image-input-empty image-input-outline" id="logo"
                                        style="background-image: url({{ asset('storage/') }})">
                                        <div class="image-input-wrapper"></div>
                                        <label
                                            class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                            data-action="change" data-toggle="tooltip" title=""
                                            data-original-title="Change avatar">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" name="image" />
                                        </label>
                                        <span
                                            class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                            data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                        <span
                                            class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                            data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                        @error('image')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="status">Status</label>
                            <div class="col-md-9">
                                <input type="hidden" checked value="0" name="status" />
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" {{ old('status', 1) ? 'checked' : null }} value="1"
                                            name="status" />
                                        <span></span>
                                    </label>
                                </span>

                                @error('status')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/dashboard/js/pages/crud/file-upload/image-input.js') }}"></script>
    <script>
        let avatar5 = new KTImageInput('logo');
        let avatar6 = new KTImageInput('footerLogo');
    </script>
@endpush
