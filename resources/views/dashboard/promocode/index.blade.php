@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Promo Codes</h3>
                    </div>

                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>Points</th>
                                <th>Code</th>
                                <th>Activr Date</th>
                                <th>Finished Date</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($promocode as $coupon)
                                <tr>
                                    <td>
                                        {{ $coupon->sl }}
                                    </td>
                                    <td>{{ $coupon->name }}</td>
                                    <td>{{ $coupon->point }}</td>
                                    <td>{{ $coupon->code }}</td>
                                    <td>{{ $coupon->active_date }}</td>
                                    <td>{{ $coupon->finished_date }}</td>                                   
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $promocode->links() }}

                </div>
            </div>
        </div>
    </div>
@endsection
