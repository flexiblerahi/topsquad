@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Create Promo Code
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" enctype="multipart/form-data" method="POST" action="{{route('promocode.store')}}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-6">

                        <div class="form-row">
                          
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="mobile1">{{ __('Name') }}</label>
                                    <input name="name" id="name" value="{{ old('name')  }}" placeholder="Name" type="text"
                                        class="form-control form-control-solid @error('name') is-invalid @enderror">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="mobile1">{{ __('Points') }}</label>
                                    <input name="point" id="point" value="{{ old('points')  }}" placeholder="points" type="number"
                                        class="form-control form-control-solid @error('name') is-invalid @enderror">
                                    @error('point')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label for="body">{{ __('Active Date') }}</label>
                                  <input name="active_date" id="active_date" value="{{ old('active_date')  }}" placeholder="Active date" type="date"
                                      class="form-control form-control-solid @error('active_date') is-invalid @enderror">
                                  @error('active_date')
                                      <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                  @enderror
                              </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label for="body">{{ __('End Date') }}</label>
                                <input name="finished_date" id="finished_date" value="{{ old('finished_date')  }}" placeholder="End Date" type="date"
                                    class="form-control form-control-solid @error('finished_date') is-invalid @enderror">
                                @error('finished_date')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        
                        </div>





                    </div>
                </div>



            </form>
        </div>
    </div>
@endsection
