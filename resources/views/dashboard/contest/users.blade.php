@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Running Contest List</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                      <thead>
                          <tr>
                              <th>SL</th>
                              <th>User Name</th>
                              <th>team name</th>
                              <th>Total Point</th>
                              <th>Created at</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach ($contest_teams as $key => $contest_team)
                            <tr>
                              <td>{{ $key++ }}</td>
                              <td>{{ $contest_team->user->username }}</td>
                              <td>{{ $contest_team->name }}</td>
                              <td>{{ $contest_team->point }}</td>
                              <td>{{ $contest_team->created_at->diffForHumans() }}</td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
