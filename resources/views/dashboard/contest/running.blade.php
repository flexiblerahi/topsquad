@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Running Contest List</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>Short Name</th>
                                <th>Entry Fee</th>
                                <th>Entry Fee Type</th>
                                <th>Match Start</th>
                                <th>Contest Ranks</th>
                                <th>User List</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contests as $contest)
                                <tr>
                                    <td>{{ $loop->iteration + $contests->firstItem() - 1 }}</td>
                                    <td>{{ $contest->name }}</td>
                                    <td>{{ $contest->shortname }}</td>
                                    <td>{{ $contest->entry_fee}}</td>
                                    <td>{{ $contest->entry_fee_type}}</td>
                                    <td>{{ $contest->match->match_started_at->diffForHumans() }}</td>
                                    <td><a href="{{ route('contest.rank.list', $contest->id) }}">Rank List</a></td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('contest.users', $contest->id) }}"><i class="fa fa-users"></i></a>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-end">{{ $contests->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@endsection
