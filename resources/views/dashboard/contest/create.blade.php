@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Create Contest
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST"
                action="{{ route('contest.store') }}">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="name">{{ __('Name') }} <span class="text-danger">*</span></label>
                            <input name="name" id="name" value="{{ old('name') }}"
                                class="form-control form-control-solid @error('name') is-invalid @enderror" type="text"
                                placeholder="Team Name">
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="shortname">{{ __('Short Name') }} <span class="text-danger">*</span></label>
                            <input name="shortname" id="shortname" value="{{ old('shortname') }}"
                                class="form-control form-control-solid @error('shortname') is-invalid @enderror" type="text"
                                placeholder="Short Name">
                            @error('shortname')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="match_id">{{ __('Choose Match') }} <span class="text-danger">*</span></label>
                            <select class="form-control " id="match_id" name="match_id">
                                <option value="">Select Match</option>
                                @foreach ($matches as $match)
                                    <option value="{{ $match->id }}">{{ $match->teamoneOne->name }} <b>vs</b>
                                        {{ $match->teamtwoOne->name }}</option>
                                @endforeach
                            </select>
                            @error('shortname')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="entry_fee">{{ __('Entry Fee') }} <span class="text-danger">*</span></label>
                            <input name="entry_fee" id="entry_fee" value="{{ old('entry_fee') }}"
                                class="form-control form-control-solid @error('entry_fee') is-invalid @enderror" type="number"
                                placeholder="Entry Fee">
                            @error('entry_fee')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="entry_fee_type">{{ __('Entry Fee Type') }} <span
                                    class="text-danger">*</span></label>
                            <div class="p-3 border-round border w-100">
                              <p class="m-0">coin</p>
                            </div>
                            @error('entry_fee_type')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="count">{{ __('Team Count') }} <span class="text-danger">*</span></label>
                            <input name="count" id="count" value="{{ old('count') }}"
                                class="form-control form-control-solid @error('count') is-invalid @enderror" type="number"
                                placeholder="Team Count">
                            @error('count')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 col-form-label" for="status">Status</label>
                          <div class="col-md-9">
                              <input type="hidden" checked value="0" name="status" />
                              <span class="switch switch-icon">
                                  <label>
                                      <input type="checkbox" {{ old('status', 1) ? 'checked' : null }} value="1"
                                          name="status" />
                                      <span></span>
                                  </label>
                              </span>

                              @error('status')
                                  <div class="text-danger">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/dashboard/js/pages/crud/file-upload/image-input.js') }}"></script>
    <script>
        let avatar5 = new KTImageInput('logo');
        let avatar6 = new KTImageInput('footerLogo');
    </script>
@endpush
