@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Slider Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Update Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST"
                action="{{ route('register-offer.update', $register_offer->id) }}">
                @csrf
                @method('PUT')
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="point">{{ __('Point') }} <span class="text-danger">*</span></label>
                            <input name="point" id="point" value="{{ $register_offer->point }}"
                                class="form-control form-control-solid @error('point') is-invalid @enderror" type="number"
                                placeholder="Point for New User">
                            @error('point')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="coin">{{ __('Coin') }} <span class="text-danger">*</span></label>
                            <input name="coin" id="coin" value="{{ $register_offer->coin }}"
                                class="form-control form-control-solid @error('coin') is-invalid @enderror" type="number"
                                placeholder="Coin for New User">
                            @error('coin')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label" for="status">Status</label>
                            <div class="col-md-9">
                                {{-- <input type="hidden"  value="0" name="status" /> --}}
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" {{ $register_offer->status ? 'checked' : null }} value="1"
                                            name="status" />
                                        <span></span>
                                    </label>
                                </span>

                                @error('status')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/dashboard/js/pages/crud/file-upload/image-input.js') }}"></script>
    <script>
        let avatar5 = new KTImageInput('logo');
        let avatar6 = new KTImageInput('footerLogo');
    </script>
@endpush
