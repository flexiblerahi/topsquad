@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Player Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <a href="{{ route('team.show', $team_id) }}" class="btn btn-success font-weight-bolder mx-4">
                        <i class="fa fa-fast-backward" aria-hidden="true"></i>
                        Back
                    </a>
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>

                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST" action="{{ route('player.team.store', $team_id) }}">
                @csrf
                <div class="row justify-content-around">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>
                            <input type="text" class="form-control" name="name" id="name" aria-describedby="nameId" value="{{ old('name') }}" placeholder="Enter Player Name">
                            <small id="nameId" class="form-text text-muted">Player name</small>
                        </div>
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="credit">{{ __('Credit') }}</label>
                            <input type="text" class="form-control" name="credit" id="credit" aria-describedby="creditId" value="{{ old('credit') }}" placeholder="Enter Player credit">
                            <small id="creditId" class="form-text text-muted">Player credit</small>
                        </div>
                        @error('credit')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-around">
                  <div class="col-lg-4">
                    <div class="mb-3">
                        <label for="position" class="form-label">{{ __('Player Position') }}</label>
                        <input type="hidden" name="position" value={{ $position }}>
                        @if ($position == App\Models\Player::BATSMAN)
                            <div class="p-3 border rounded w-100">
                                <p class="m-0">Batsman</p>
                            </div>
                        @elseif ($position == App\Models\Player::BOWLER)
                            <div class="p-3 border rounded w-100">
                                <p class="m-0">Bowler</p>
                            </div>
                        @elseif ($position == App\Models\Player::WICKETKEEPER)
                            <div class="p-3 border rounded w-100">
                                <p class="m-0">Wicket Keeper</p>
                            </div>
                        @elseif ($position == App\Models\Player::ALLROUNDER)
                            <div class="p-3 border rounded w-100">
                                <p class="m-0">All Rounder</p>
                            </div>
                        @endif
                    </div>
                  </div>
                  <div class="col-lg-4">
                      <div class="form-group">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="is_announce" id="announce" value="1" checked>
                            Announce
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
            </form>
        </div>
    </div>
@endsection
