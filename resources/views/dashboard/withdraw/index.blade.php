@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Pending Request</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>User Balance</th>
                                <th>Request amount</th>
                                <th>Mobile</th>
                                <th>Method</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pending_requests as $key => $pending_request)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $pending_request->user->username }}</td>
                                    <td>{{ $pending_request->user->money }}</td>
                                    <td>{{ $pending_request->amount }}</td>
                                    <td>{{ $pending_request->mobile }}</td>
                                    <td>{{ $pending_request->method }}</td>
                                    <td nowrap="nowrap">
                                        <form method="post"
                                        action="{{ route('withdraw.update', $pending_request->id) }}"
                                            class="d-inline-block">
                                            @csrf
                                            @method('PUT')
                                            <input type="hidden" name="user_id" value="{{ $pending_request->user->id }}">
                                            <button type="submit"
                                                onclick="return confirm('Are you sure you want to approve this Withdraw?');"
                                                class="btn btn-success">
                                                Approved
                                            </button>
                                        </form>
                                        <form method="post"
                                        action="{{ route('cancel.withdraw', $pending_request->id) }}"
                                            class="d-inline-block">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit"
                                                onclick="return confirm('Are you sure you want to cancel this Withdraw?');"
                                                class="btn btn-danger">
                                                Cancel
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
