@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Approve Request</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>User Balance</th>
                                <th>Request amount</th>
                                <th>Mobile</th>
                                <th>Method</th>
                                <th>Approved at</th>
                                <th>Status</th>


                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($approve_requests as $key => $pending_request)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $pending_request->user->username }}</td>
                                    <td>{{ $pending_request->user->money }}</td>
                                    <td>{{ $pending_request->amount }}</td>
                                    <td>{{ $pending_request->mobile }}</td>
                                    <td>{{ $pending_request->method }}</td>
                                    <td>{{ $pending_request->approve_at->diffForHumans() }}</td>

                                    {{-- <td>{{ $pending_request->status == App\Models\Withdraw::APPROVE ? "Approved" : "Cancel"}}</td>
                                     --}}
                                    <td>@if ($pending_request->status == App\Models\Withdraw::APPROVE)
                                      <p class="btn bg-success text-white">Withdraw success</p>
                                    @else
                                      <p class=" btn bg-danger text-white">Withdraw cancel</p>
                                    @endif</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
