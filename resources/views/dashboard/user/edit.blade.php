@extends('layouts.dashboard')
@section('content')
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">
                Edit User
            </h3>
        </div>
        <div class="card-toolbar">
            <div class="btn-group">
                <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                    <i class="ki ki-check icon-sm"></i>
                    Save Form
                </button>
            </div>
        </div>
    </div>
    <!--begin::Portlet-->
    <div class="card-body">
        <form class="form" id="kt_form" enctype="multipart/form-data" method="POST"
            action="{{ route('user.update', $users->id) }}">
            @csrf
            @method('PUT')
            <div class="row justify-content-center">
                <div class="col-lg-6">

                    <div class="form-group">
                        <label for="mobile1">{{ __('UserName') }}</label>
                        <input name="username" id="mobile1"
                            value="{{ old('username') ?? $users->username ?? 'Username' }}" placeholder="username"
                            type="text" class="form-control form-control-solid @error('username') is-invalid @enderror">
                        @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mobile1">{{ __('First Name') }}</label>
                                <input name="first_name" id="mobile1"
                                    value="{{ old('first_name') ?? $users->first_name ?? 'Fisrt Name'}}"
                                    placeholder="First Name" type="text"
                                    class="form-control form-control-solid @error('firstname') is-invalid @enderror">
                                @error('firstname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mobile1">{{ __('Lastname') }}</label>
                                <input name="last_name" id="mobile2"
                                    value="{{ old('last_name') ?? $users->last_name ?? 'Last name' }}"
                                    placeholder="Ex: Last Name" type="text"
                                    class="form-control form-control-solid @error('lastname') is-invalid @enderror">
                                @error('last_name')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mobile1">{{ __('Email') }}</label>
                                <input name="email" id="email" value="{{ old('email') ?? $users->email ?? 'Email' }}"
                                    placeholder="Ex: demo@gmail.com" type="email"
                                    class="form-control form-control-solid @error('email') is-invalid @enderror">
                                @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mobile1">{{ __('Phone Number') }}</label>
                                <input name="phone" id="phone" value="{{ old('phone') ?? $users->phone ?? 'Number' }}"
                                    placeholder="Ex: 01xxxxxxxxx" type="number"
                                    class="form-control form-control-solid @error('phone') is-invalid @enderror">
                                @error('phone')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mobile1">{{ __('Password') }}</label>
                                <input name="password" id="phone" value="{{ old('password')  }}" placeholder="Ex:123456"
                                    type="password"
                                    class="form-control form-control-solid @error('password') is-invalid @enderror">
                                @error('password')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="mobile1">{{ __('Confirm Password') }}</label>
                                <input name="password_confirmation" id="phone"
                                    value="{{ old('password_confirmation')  }}" placeholder="Ex: 123456" type="password"
                                    class="form-control form-control-solid @error('password_confirmation') is-invalid @enderror">
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('assets/dashboard/js/pages/crud/file-upload/image-input.js') }}"></script>
<script>
    let avatar5 = new KTImageInput('logo');
        let avatar6 = new KTImageInput('footerLogo');
</script>
@endpush
