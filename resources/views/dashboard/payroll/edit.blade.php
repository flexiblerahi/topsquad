@extends('layouts.dashboard')
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Package Information
                </h3>
            </div>
            <div class="card-toolbar">
                <div class="btn-group">
                    <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
                        <i class="ki ki-check icon-sm"></i>
                        Save Form
                    </button>
                </div>
            </div>
        </div>
        <!--begin::Portlet-->
        <div class="card-body">
            <form class="form" id="kt_form" method="POST" action="{{ route('admin_payroll.update', $adminPayroll->id) }}">
                @csrf
                @method('PUT')
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="form-group">
                          <label for="mobile">Mobile</label>
                          <input type="text"
                            class="form-control" name="mobile" value="{{ $adminPayroll->mobile }}" id="mobile" aria-describedby="mobileId" placeholder="Enter Mobile/Accountg Number">
                          <small id="nameId" class="form-text text-muted">Phone/Account Number</small>
                        </div>
                        @error('mobile')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-around">
                    <div class="col-lg-4">
                      {{-- start  --}}
                        <div class="form-group">
                          <label for="methodId">Method</label>
                          <select class="form-control" name="method" id="methodId">
                              <option value="bkash" @if ($adminPayroll->method == "bkash") selected @endif>Bkash -BRAC Bank Limited</option>
                              <option value="nagad" @if ($adminPayroll->method == "nagad") selected @endif>Nagad -Bangladesh Post Office</option>
                          </select>
                        </div>
                        @error('method')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  <div class="col-lg-11  offset-lg-1 d-flex">
                    <label class="col-form-label mr-2" for="status">Status</label>
                      <input type="hidden" checked value="0" name="status" />
                      <span class="switch switch-icon">
                          <label>
                              <input type="checkbox" @if($adminPayroll->status == 1) checked @endif value="1"
                                  name="status" />
                              <span></span>
                          </label>
                      </span>
                      @error('status')
                          <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                </div>
            </form>
        </div>
    </div>
@endsection
