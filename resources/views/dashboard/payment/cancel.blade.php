@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">League List</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>User Name</th>
                                <th>Transaction ID</th>
                                <th>Approved At</th>
                                <th>Requested At</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $key => $payment)
                                <tr>
                                    <td>{{ $loop->iteration + $payments->firstItem() - 1 }}</td>
                                    <td><h4>{{ $payment->user->username }}</h4></td>
                                    <td>{{ $payment->transaction_id }}</td>
                                    <td>{{ $payment->amount }}</td>
                                    <td>{{ $payment->approve_at->format('d M Y h:i A') }}</td>
                                    <td>{{ $payment->created_at->format('d M Y h:i A') }}</td>
                                    <td nowrap="nowrap">
                                        <form method="post" action="{{ route('payment.destroy', $payment->id) }}"
                                          class="d-inline-block">
                                          @csrf
                                          @method('DELETE')
                                          <button type="submit"
                                              onclick="return confirm('Are you sure you want to delete?');"
                                              class="btn btn-light btn-hover-primary mr-3">
                                              Delete
                                          </button>
                                      </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $payments->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection



