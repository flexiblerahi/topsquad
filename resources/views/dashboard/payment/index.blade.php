@extends('layouts.dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Payment List</h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>User Name</th>
                                <th>User Balance</th>
                                <th>Transaction ID</th>
                                <th>Method</th>
                                <th>Amount</th>

                                <th>Requested At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $key => $payment)
                                <tr>
                                    <td>{{ $loop->iteration + $payments->firstItem() - 1 }}</td>
                                    <td>{{ $payment->user->username }}</td>
                                    <td>{{ $payment->user->money }}</td>
                                    <td>{{ $payment->transaction_id == null ? 'From profile balance' : $payment->transaction_id }}</td>
                                    <td>{{ $payment->method }}</td>
                                    <td>{{ $payment->amount }}</td>
                                    <td>{{ $payment->created_at->format('d M Y h:i A') }}</td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('payment.edit', $payment->id) }}"
                                            class="btn btn-light btn-hover-primary mx-3">
                                            approve
                                        </a>
                                        <a href="{{ route('payment.cancel', $payment->id) }}"
                                            class="btn btn-light btn-hover-primary mr-3">
                                            cancel
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $payments->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection



