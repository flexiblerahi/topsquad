<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->string('transaction_id')->nullable();
            $table->string('mobile')->nullable();
            $table->foreignId('admin_payroll_id')->nullable()->constrained('admin_payrolls')->cascadeOnDelete();
            $table->double('amount')->nullable();
            $table->string('method');
            $table->foreignId('package_id')->constrained('packages')->cascadeOnDelete();
            $table->tinyInteger('status')->default(0);
            $table->timestamp('approve_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
