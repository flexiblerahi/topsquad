<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserContestRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_contest_ranks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('contest_rank_id')->nullable()->constrained('contest_ranks');
            $table->integer('position');
            $table->double('totalpoints')->default(0);
            $table->foreignId('contest_id')->constrained('contests')->cascadeOnDelete();
            $table->foreignId('match_team_id')->constrained('match_teams')->cascadeOnDelete();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->tinyInteger('isrank')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_contest_ranks');
    }
}
