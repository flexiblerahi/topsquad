<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_teams', function (Blueprint $table) {
            $table->id();
            $table->foreignId('league_id')->constrained('leagues')->cascadeOnDelete();
            $table->foreignId('team_one_id')->constrained('teams')->cascadeOnDelete();
            $table->integer('team_one_run')->default(0);
            $table->tinyInteger('team_one_wicket')->default(0);
            $table->double('team_one_over', 20, 2)->default(0.00);
            $table->foreignId('team_two_id')->constrained('teams')->cascadeOnDelete();
            $table->integer('team_two_run')->default(0);
            $table->tinyInteger('team_two_wicket')->default(0);
            $table->double('team_two_over', 20, 2)->default(0.00);
            $table->timestamp('match_started_at');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_teams');
    }
}
