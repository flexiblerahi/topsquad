<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contest_teams', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('contest_id')->constrained('contests')->cascadeOnDelete();
            $table->foreignId('vice_captain')->constrained('players')->cascadeOnDelete();
            $table->foreignId('captain')->constrained('players')->cascadeOnDelete();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->foreignId('match_team_id')->constrained('match_teams')->cascadeOnDelete();
            $table->json('players');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contest_teams');
    }
}
