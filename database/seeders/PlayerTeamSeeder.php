<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlayerTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('player_team')->insert([
            [
                "player_id" => 1,
                "team_id" => 1
            ],
            [
                "player_id" => 2,
                "team_id" => 1
            ],
            [
                "player_id" => 3,
                "team_id" => 1
            ],
            [
                "player_id" => 4,
                "team_id" => 1
            ],
            [
                "player_id" => 5,
                "team_id" => 1
            ],
            [
                "player_id" => 6,
                "team_id" => 1
            ],
            [
                "player_id" => 7,
                "team_id" => 1
            ],
            [
                "player_id" => 8,
                "team_id" => 1
            ],
            [
                "player_id" => 9,
                "team_id" => 1
            ],
            [
                "player_id" => 10,
                "team_id" => 1
            ],
            [
                "player_id" => 11,
                "team_id" => 1
            ],
            [
                "player_id" => 12,
                "team_id" => 2
            ],
            [
                "player_id" => 13,
                "team_id" => 2
            ],
            [
                "player_id" => 14,
                "team_id" => 2
            ],
            [
                "player_id" => 15,
                "team_id" => 2
            ],
            [
                "player_id" => 16,
                "team_id" => 2
            ],
            [
                "player_id" => 17,
                "team_id" => 2
            ],
            [
                "player_id" => 18,
                "team_id" => 2
            ],
            [
                "player_id" => 19,
                "team_id" => 2
            ],
            [
                "player_id" => 20,
                "team_id" => 2
            ],
            [
                "player_id" => 21,
                "team_id" => 2
            ],
            [
                "player_id" => 22,
                "team_id" => 2
            ],
            [
                "player_id" => 23,
                "team_id" => 3
            ],
            [
                "player_id" => 24,
                "team_id" => 3
            ],
            [
                "player_id" => 25,
                "team_id" => 3
            ],
            [
                "player_id" => 26,
                "team_id" => 3
            ],
            [
                "player_id" => 27,
                "team_id" => 3
            ],
            [
                "player_id" => 28,
                "team_id" => 3
            ],
            [
                "player_id" => 29,
                "team_id" => 3
            ],
            [
                "player_id" => 30,
                "team_id" => 3
            ],
            [
                "player_id" => 31,
                "team_id" => 3
            ],
            [
                "player_id" => 32,
                "team_id" => 3
            ],
            [
                "player_id" => 33,
                "team_id" => 3
            ],
            [
                "player_id" => 34,
                "team_id" => 4
            ],
            [
                "player_id" => 35,
                "team_id" => 4
            ],
            [
                "player_id" => 36,
                "team_id" => 4
            ],
            [
                "player_id" => 37,
                "team_id" => 4
            ],
            [
                "player_id" => 38,
                "team_id" => 4
            ],
            [
                "player_id" => 39,
                "team_id" => 4
            ],
            [
                "player_id" => 40,
                "team_id" => 4
            ],
            [
                "player_id" => 41,
                "team_id" => 4
            ],
            [
                "player_id" => 42,
                "team_id" => 4
            ],
            [
                "player_id" => 43,
                "team_id" => 4
            ],
            [
                "player_id" => 44,
                "team_id" => 4
            ],
            [
                "player_id" => 45,
                "team_id" => 5
            ],
            [
                "player_id" => 46,
                "team_id" => 5
            ],
            [
                "player_id" => 47,
                "team_id" => 5
            ],
            [
                "player_id" => 48,
                "team_id" => 5
            ],
            [
                "player_id" => 49,
                "team_id" => 5
            ],
            [
                "player_id" => 50,
                "team_id" => 5
            ],
            [
                "player_id" => 51,
                "team_id" => 5
            ],
            [
                "player_id" => 52,
                "team_id" => 5
            ],
            [
                "player_id" => 53,
                "team_id" => 5
            ],
            [
                "player_id" => 54,
                "team_id" => 5
            ],
            [
                "player_id" => 55,
                "team_id" => 5
            ],
            [
                "player_id" => 56,
                "team_id" => 6
            ],
            [
                "player_id" => 57,
                "team_id" => 6
            ],
            [
                "player_id" => 58,
                "team_id" => 6
            ],
            [
                "player_id" => 59,
                "team_id" => 6
            ],
            [
                "player_id" => 60,
                "team_id" => 6
            ],
            [
                "player_id" => 61,
                "team_id" => 6
            ],
            [
                "player_id" => 62,
                "team_id" => 6
            ],
            [
                "player_id" => 63,
                "team_id" => 6
            ],
            [
                "player_id" => 64,
                "team_id" => 6
            ],
            [
                "player_id" => 65,
                "team_id" => 6
            ],
            [
                "player_id" => 66,
                "team_id" => 6
            ],
            [
                "player_id" => 67,
                "team_id" => 7
            ],
            [
                "player_id" => 68,
                "team_id" => 7
            ],
            [
                "player_id" => 69,
                "team_id" => 7
            ],
            [
                "player_id" => 70,
                "team_id" => 7
            ],
            [
                "player_id" => 71,
                "team_id" => 7
            ],
            [
                "player_id" => 72,
                "team_id" => 7
            ],
            [
                "player_id" => 73,
                "team_id" => 7
            ],
            [
                "player_id" => 74,
                "team_id" => 7
            ],
            [
                "player_id" => 75,
                "team_id" => 7
            ],
            [
                "player_id" => 76,
                "team_id" => 7
            ],
            [
                "player_id" => 77,
                "team_id" => 7
            ],
            [
                "player_id" => 78,
                "team_id" => 10
            ],
            [
                "player_id" => 79,
                "team_id" => 10
            ],
            [
                "player_id" => 80,
                "team_id" => 10
            ],
            [
                "player_id" => 81,
                "team_id" => 10
            ],
            [
                "player_id" => 82,
                "team_id" => 10
            ],
            [
                "player_id" => 83,
                "team_id" => 10
            ],
            [
                "player_id" => 84,
                "team_id" => 10
            ],
            [
                "player_id" => 85,
                "team_id" => 10
            ],
            [
                "player_id" => 86,
                "team_id" => 10
            ],
            [
                "player_id" => 87,
                "team_id" => 10
            ],
            [
                "player_id" => 88,
                "team_id" => 10
            ],
            [
                "player_id" => 89,
                "team_id" => 12
            ],
            [
                "player_id" => 90,
                "team_id" => 12
            ],
            [
                "player_id" => 91,
                "team_id" => 12
            ],
            [
                "player_id" => 92,
                "team_id" => 12
            ],
            [
                "player_id" => 93,
                "team_id" => 12
            ],
            [
                "player_id" => 94,
                "team_id" => 12
            ],
            [
                "player_id" => 95,
                "team_id" => 12
            ],
            [
                "player_id" => 96,
                "team_id" => 12
            ],
            [
                "player_id" => 97,
                "team_id" => 12
            ],
            [
                "player_id" => 98,
                "team_id" => 12
            ],
            [
                "player_id" => 99,
                "team_id" => 12
            ],
            [
                "player_id" => 100,
                "team_id" => 11
            ],
            [
                "player_id" => 101,
                "team_id" => 11
            ],
            [
                "player_id" => 102,
                "team_id" => 11
            ],
            [
                "player_id" => 103,
                "team_id" => 11
            ],
            [
                "player_id" => 104,
                "team_id" => 11
            ],
            [
                "player_id" => 105,
                "team_id" => 11
            ],
            [
                "player_id" => 106,
                "team_id" => 11
            ],
            [
                "player_id" => 107,
                "team_id" => 11
            ],
            [
                "player_id" => 108,
                "team_id" => 11
            ],
            [
                "player_id" => 109,
                "team_id" => 11
            ],
            [
                "player_id" => 110,
                "team_id" => 11
            ],
            [
                "player_id" => 111,
                "team_id" => 8
            ],
            [
                "player_id" => 112,
                "team_id" => 8
            ],
            [
                "player_id" => 113,
                "team_id" => 8
            ],
            [
                "player_id" => 114,
                "team_id" => 8
            ],
            [
                "player_id" => 115,
                "team_id" => 8
            ],
            [
                "player_id" => 116,
                "team_id" => 8
            ],
            [
                "player_id" => 117,
                "team_id" => 8
            ],
            [
                "player_id" => 118,
                "team_id" => 8
            ],
            [
                "player_id" => 119,
                "team_id" => 8
            ],
            [
                "player_id" => 120,
                "team_id" => 8
            ],
            [
                "player_id" => 121,
                "team_id" => 8
            ],
            [
                "player_id" => 122,
                "team_id" => 9
            ],
            [
                "player_id" => 123,
                "team_id" => 9
            ],
            [
                "player_id" => 124,
                "team_id" => 9
            ],
            [
                "player_id" => 125,
                "team_id" => 9
            ],
            [
                "player_id" => 126,
                "team_id" => 9
            ],
            [
                "player_id" => 127,
                "team_id" => 9
            ],
            [
                "player_id" => 128,
                "team_id" => 9
            ],
            [
                "player_id" => 129,
                "team_id" => 9
            ],
            [
                "player_id" => 130,
                "team_id" => 9
            ],
            [
                "player_id" => 131,
                "team_id" => 9
            ],
            [
                "player_id" => 132,
                "team_id" => 9
            ]
        ]);
    }
}
