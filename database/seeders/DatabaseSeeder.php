<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  public function run(): void
  {
    $this->call(CoreDataSeeder::class);
    $this->call(DemoDataSeeder::class);
    $this->call(LeagueSeeder::class);
    $this->call(TeamSeeder::class);
    $this->call(PlayerSeeder::class);
    $this->call(PlayerTeamSeeder::class);
    $this->call(TeamLeagueSeeder::class);

  }
}
