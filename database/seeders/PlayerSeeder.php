<?php

namespace Database\Seeders;

use App\Models\Player;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $players = [
            [
                'name' => 'Sakib Al hasan',
                'position' => Player::ALLROUNDER,
                'credit' => 1.3,
            ], [
                'name' => 'Tamim Iqbal',
                'position' => Player::BATSMAN,
                'credit' => 1.6,
            ], [
                'name' => 'Musfiqur Rahim',
                'position' => Player::WICKETKEEPER,
                'credit' => 5.6,
            ], [
                'name' => 'Taijul Islam',
                'position' => Player::BOWLER,
                'credit' => 1.0,
            ], [
                'name' => 'Mustafizur Rahman',
                'position' => Player::BOWLER,
                'credit' => 2.8,
            ], [
                'name' => 'Afif Hossain Dhrubo',
                'position' => Player::ALLROUNDER,
                'credit' => 3.8,
            ], [
                'name' => 'Mehidy Hasan Miraz',
                'position' => Player::ALLROUNDER,
                'credit' => 2.8,
            ], [
                'name' => 'Mohammad Rubel Hossain',
                'position' => Player::BOWLER,
                'credit' => 3.2,
            ], [
                'name' => 'Shamim Hossain Patwari',
                'position' => Player::ALLROUNDER,
                'credit' => 4.2,
            ], [
                'name' => 'Liton Kumar Das',
                'position' => Player::WICKETKEEPER,
                'credit' => 1.2,
            ], [
                'name' => 'Taskin Ahmed',
                'position' => Player::BOWLER,
                'credit' => 2.2,
            ], [
                'name' => 'MS. Dhoni',
                'position' => Player::WICKETKEEPER,
                'credit' => 9.3,
            ], [
                'name' => 'Rabindra Jadeja',
                'position' => Player::ALLROUNDER,
                'credit' => 4.6,
            ], [
                'name' => 'Virat Kohli',
                'position' => Player::BATSMAN,
                'credit' => 2.3,
            ], [
                'name' => 'KL Rahul',
                'position' => Player::ALLROUNDER,
                'credit' => 6.3,
            ], [
                'name' => 'Rishabh Rajendra Pant',
                'position' => Player::WICKETKEEPER,
                'credit' => 6.3,
            ], [
                'name' => 'Jasprit Jasbirsingh Bumrah',
                'position' => Player::BOWLER,
                'credit' => 6.3,
            ], [
                'name' => ' Mayank Anurag Agarwal',
                'position' => Player::BATSMAN,
                'credit' => 6.3,
            ], [
                'name' => 'ROHIT sharma',
                'position' => Player::BATSMAN,
                'credit' => 6.3,
            ], [
                'name' => 'Bhuvneshwar Kumar',
                'position' => Player::BOWLER,
                'credit' => 6.1,
            ], [
                'name' => 'Mohammed Siraj',
                'position' => Player::BOWLER,
                'credit' => 6.6,
            ], [
                'name' => 'Mohammed Shami Ahmed',
                'position' => Player::BOWLER,
                'credit' => 7.6,
            ], [
                'name' => 'Mitchell Starc',
                'position' => Player::BOWLER,
                'credit' => 7.8,
            ], [
                'name' => 'Steven Smith',
                'position' => Player::BATSMAN,
                'credit' => 7.8,
            ], [
                'name' => 'Josh Hazlewood',
                'position' => Player::BOWLER,
                'credit' => 5.8,
            ], [
                'name' => 'Marnus Labuschagne',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Alex Carey',
                'position' => Player::WICKETKEEPER,
                'credit' => 8.8,
            ], [
                'name' => 'David Warner',
                'position' => Player::BATSMAN,
                'credit' => 5.8,
            ], [
                'name' => 'Pat cummins',
                'position' => Player::ALLROUNDER,
                'credit' => 5.8,
            ], [
                'name' => 'Nathan Lyon',
                'position' => Player::BOWLER,
                'credit' => 5.8,
            ], [
                'name' => 'Usman Khawaja',
                'position' => Player::BATSMAN,
                'credit' => 5.8,
            ], [
                'name' => 'Michael Neser',
                'position' => Player::BOWLER,
                'credit' => 2.8,
            ], [
                'name' => 'Jhye Richardson',
                'position' => Player::BOWLER,
                'credit' => 2.8,
            ], [
                'name' => 'Martin Guptill',
                'position' => Player::BATSMAN,
                'credit' => 2.0,
            ], [
                'name' => 'Ross Taylor',
                'position' => Player::BATSMAN,
                'credit' => 8.0,
            ], [
                'name' => 'Kane Williamson',
                'position' => Player::BATSMAN,
                'credit' => 5.0,
            ], [
                'name' => 'Will Young ',
                'position' => Player::BATSMAN,
                'credit' => 5.0,
            ], [
                'name' => 'Finn Allen',
                'position' => Player::BATSMAN,
                'credit' => 8.0,
            ], [
                'name' => 'Tom Blundell',
                'position' => Player::WICKETKEEPER,
                'credit' => 6.0,
            ], [
                'name' => 'Devon Conway',
                'position' => Player::WICKETKEEPER,
                'credit' => 5.0,
            ], [
                'name' => 'Todd Astle',
                'position' => Player::ALLROUNDER,
                'credit' => 8.0,
            ], [
                'name' => 'Colin de Grandhomme',
                'position' => Player::ALLROUNDER,
                'credit' => 4.0,
            ], [
                'name' => 'Tim Southee',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Trent Boult',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Akeal Hosein',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Kraigg Brathwaite',
                'position' => Player::ALLROUNDER,
                'credit' => 5.0,
            ], [
                'name' => 'Andre Fletcher',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Chris Gayle',
                'position' => Player::BATSMAN,
                'credit' => 9.9,
            ], [
                'name' => 'Evin Lewis',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Kieran Powell',
                'position' => Player::BATSMAN,
                'credit' => 4.4,
            ], [
                'name' => 'Lendl Simmons',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Stinker Dams',
                'position' => Player::BOWLER,
                'credit' => 1.0,
            ], [
                'name' => 'Darren Bravo',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Shimron Hetmyer',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Ravi Rampaul',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'Dimuth Karunaratne',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Dhananjaya de Silva',
                'position' => Player::BATSMAN,
                'credit' => 0.5,
            ], [
                'name' => 'Kusal Perera',
                'position' => Player::WICKETKEEPER,
                'credit' => 5.5,
            ], [
                'name' => 'Angelo Mathews',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Charith Asalanka',
                'position' => Player::ALLROUNDER,
                'credit' => 5.0,
            ], [
                'name' => 'Dinesh Chandimal',
                'position' => Player::WICKETKEEPER,
                'credit' => 6.0,
            ], [
                'name' => 'Kamindu Mendis',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Suranga Lakmal',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'Vishwa Fernando',
                'position' => Player::BOWLER,
                'credit' => 5.0,
            ], [
                'name' => 'Kasun Rajitha',
                'position' => Player::BOWLER,
                'credit' => 8.8,
            ], [
                'name' => 'Akila Dananjaya',
                'position' => Player::BOWLER,
                'credit' => 8.0,
            ], [
                'name' => 'Asghar Afghan',
                'position' => Player::BATSMAN,
                'credit' => 3.5,
            ], [
                'name' => 'Dawlat Ahmadzai',
                'position' => Player::BOWLER,
                'credit' => 5.0,
            ], [
                'name' => 'Hamid Hassan',
                'position' => Player::BOWLER,
                'credit' => 5.0,
            ], [
                'name' => 'Hasti Gul',
                'position' => Player::BATSMAN,
                'credit' => 6.0,
            ], [
                'name' => 'Mohammad Nabi',
                'position' => Player::ALLROUNDER,
                'credit' => 5.2,
            ], [
                'name' => 'Mohammad Shahzad',
                'position' => Player::WICKETKEEPER,
                'credit' => 5.5,
            ], [
                'name' => 'Dawlat Zadran',
                'position' => Player::BOWLER,
                'credit' => 5.0,
            ], [
                'name' => 'Gulbadin Naib',
                'position' => Player::ALLROUNDER,
                'credit' => 5.0,
            ], [
                'name' => 'Istiyaz Mehjab',
                'position' => Player::BOWLER,
                'credit' => 1.0,
            ], [
                'name' => 'Rahmat Shah',
                'position' => Player::BATSMAN,
                'credit' => 5.2,
            ], [
                'name' => 'Rashid Khan',
                'position' => Player::ALLROUNDER,
                'credit' => 9.9,
            ], [
                'name' => 'Paul Strang',
                'position' => Player::ALLROUNDER,
                'credit' => 5.0,
            ], [
                'name' => 'Stuart Carlisle',
                'position' => Player::BATSMAN,
                'credit' => 5.0,
            ], [
                'name' => 'Iain Butchart',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Hamilton Masakadza',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Douglas Hondo',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'Sean Ervine',
                'position' => Player::ALLROUNDER,
                'credit' => 4.4,
            ], [
                'name' => 'Wellington Masakadza',
                'position' => Player::BOWLER,
                'credit' => 8.8,
            ], [
                'name' => 'Finlizing',
                'position' => Player::BOWLER,
                'credit' => 1.0,
            ], [
                'name' => 'taurai Muzarabani',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'Ryan Murray',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Tinashe Kamunhukamwe',
                'position' => Player::BATSMAN,
                'credit' => 5.2,
            ], [
                'name' => 'Morné Morkel',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Hashim Amla',
                'position' => Player::BATSMAN,
                'credit' => 9.9,
            ], [
                'name' => 'Paul Harris',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'Ryan McLaren',
                'position' => Player::ALLROUNDER,
                'credit' => 7.7,
            ], [
                'name' => 'David Miller',
                'position' => Player::WICKETKEEPER,
                'credit' => 8.8,
            ], [
                'name' => 'Faf du Plessis',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Imran Tahir',
                'position' => Player::BOWLER,
                'credit' => 8.8,
            ], [
                'name' => 'Dean Elgar',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Quinton de Kock',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Chris Morris',
                'position' => Player::ALLROUNDER,
                'credit' => 8.8,
            ], [
                'name' => 'Kagiso Rabada',
                'position' => Player::BOWLER,
                'credit' => 7.7,
            ], [
                'name' => 'Babar Azam',
                'position' => Player::BATSMAN,
                'credit' => 9.9,
            ], [
                'name' => 'Shadab Khan',
                'position' => Player::ALLROUNDER,
                'credit' => 7.7,
            ], [
                'name' => 'Mohammad Rizwan',
                'position' => Player::ALLROUNDER,
                'credit' => 6.6,
            ], [
                'name' => 'Haider Ali',
                'position' => Player::BOWLER,
                'credit' => 5.3,
            ], [
                'name' => 'Abdullah Shafique',
                'position' => Player::BATSMAN,
                'credit' => 4.4,
            ], [
                'name' => 'Imam-ul-Haq',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Fakhar Zaman',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Sarfaraz Ahmed',
                'position' => Player::WICKETKEEPER,
                'credit' => 5.5,
            ], [
                'name' => 'Shaheen Afridi',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Mohammad Amir',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Hasan Ali',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Rory Burns',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Ben Duckett',
                'position' => Player::BATSMAN,
                'credit' => 2.2,
            ], [
                'name' => 'Haseeb Hameed',
                'position' => Player::BATSMAN,
                'credit' => 7.7,
            ], [
                'name' => 'Eoin Morgan',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Joe Root',
                'position' => Player::ALLROUNDER,
                'credit' => 8.8,
            ], [
                'name' => 'Moeen Ali',
                'position' => Player::ALLROUNDER,
                'credit' => 8.8,
            ], [
                'name' => 'Sam Curran',
                'position' => Player::ALLROUNDER,
                'credit' => 8.8,
            ], [
                'name' => 'Ben Stokes',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Jos Buttler',
                'position' => Player::WICKETKEEPER,
                'credit' => 9.0,
            ], [
                'name' => 'James Anderson',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Jofra Archer',
                'position' => Player::BOWLER,
                'credit' => 9.9,
            ], [
                'name' => 'Kenny Carroll',
                'position' => Player::BATSMAN,
                'credit' => 5.5,
            ], [
                'name' => 'Boyd Rankin',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'Thinus Fourie',
                'position' => Player::BOWLER,
                'credit' => 8.8,
            ], [
                'name' => 'Andrew Britton',
                'position' => Player::BOWLER,
                'credit' => 5.5,
            ], [
                'name' => 'George Dockrell',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'James Hall',
                'position' => Player::ALLROUNDER,
                'credit' => 6.0,
            ], [
                'name' => 'Nigel Jones',
                'position' => Player::BOWLER,
                'credit' => 8.8,
            ], [
                'name' => 'Rory McCann',
                'position' => Player::WICKETKEEPER,
                'credit' => 5.5,
            ], [
                'name' => 'Andrew Balbirnie',
                'position' => Player::BATSMAN,
                'credit' => 8.8,
            ], [
                'name' => 'Albert van der Merwe',
                'position' => Player::ALLROUNDER,
                'credit' => 5.5,
            ], [
                'name' => 'Max Sorensen',
                'position' => Player::BOWLER,
                'credit' => 6.3,
            ]
        ];

        foreach ($players as  $player) {
            $new_player = new Player();
            $new_player->name = $player['name'];
            $new_player->position = $player['position'];
            $new_player->credit = $player['credit'];
            $new_player->is_announce = (bool) mt_rand(0, 1);
            $new_player->point = $new_player->is_announce == 0 ? 0 : random_int(10, 40);
            $new_player->save();
        }
    }
}
