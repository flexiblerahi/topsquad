<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DemoDataSeeder extends Seeder
{
    public function run(): void
    {
        $this->createDemoUsers();
    }

    private function createDemoUsers()
    {
        User::create([
            'email'             => 'user@gmail.com',
            'username'          => 'user1', 
            'email_verified_at' => now(),
            'coin'              => 2000,
            'money'             => 3000,
            'point'             => 2000,
            'phone'             => '12312312312',
            'password'          => Hash::make('123123123'),
            'remember_token'    => Str::random(10),
        ])->assignRole('User');
    }
}
