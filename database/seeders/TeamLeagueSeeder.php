<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamLeagueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('league_team')->insert([
            [
                "league_id" => 1,
                "team_id" => 1
            ],
            [
                "league_id" => 1,
                "team_id" => 2
            ],
            [
                "league_id" => 1,
                "team_id" => 3
            ],
            [
                "league_id" => 1,
                "team_id" => 4
            ],
            [
                "league_id" => 1,
                "team_id" => 5
            ],
            [
                "league_id" => 1,
                "team_id" => 6
            ],
            [
                "league_id" => 1,
                "team_id" => 7
            ],
            [
                "league_id" => 1,
                "team_id" => 8
            ],
            [
                "league_id" => 1,
                "team_id" => 9
            ],
            [
                "league_id" => 1,
                "team_id" => 10
            ],
            [
                "league_id" => 1,
                "team_id" => 11
            ],
            [
                "league_id" => 2,
                "team_id" => 12
            ],
            [
                "league_id" => 2,
                "team_id" => 13
            ],
            [
                "league_id" => 2,
                "team_id" => 14
            ],
            [
                "league_id" => 2,
                "team_id" => 15
            ],
            [
                "league_id" => 2,
                "team_id" => 16
            ],
            [
                "league_id" => 2,
                "team_id" => 17
            ],
            [
                "league_id" => 2,
                "team_id" => 18
            ],
            [
                "league_id" => 2,
                "team_id" => 19
            ],
            [
                "league_id" => 2,
                "team_id" => 20
            ],
            [
                "league_id" => 3,
                "team_id" => 21
            ],
            [
                "league_id" => 3,
                "team_id" => 22
            ],
            [
                "league_id" => 3,
                "team_id" => 23
            ],
            [
                "league_id" => 3,
                "team_id" => 24
            ],
            [
                "league_id" => 3,
                "team_id" => 25
            ],
            [
                "league_id" => 1,
                "team_id" => 26
            ],
            [
                "league_id" => 3,
                "team_id" => 27
            ],
            [
                "league_id" => 3,
                "team_id" => 28
            ],
            [
                "league_id" => 5,
                "team_id" => 29
            ],
            [
                "league_id" => 5,
                "team_id" => 30
            ],
            [
                "league_id" => 5,
                "team_id" => 31
            ],
            [
                "league_id" => 5,
                "team_id" => 32
            ],
            [
                "league_id" => 5,
                "team_id" => 33
            ],
            [
                "league_id" => 5,
                "team_id" => 34
            ],
            [
                "league_id" => 4,
                "team_id" => 35
            ],
            [
                "league_id" => 4,
                "team_id" => 36
            ],
            [
                "league_id" => 4,
                "team_id" => 37
            ],
            [
                "league_id" => 4,
                "team_id" => 38
            ],
            [
                "league_id" => 4,
                "team_id" => 39
            ],
            [
                "league_id" => 4,
                "team_id" => 40
            ]
        ]);
    }
}
