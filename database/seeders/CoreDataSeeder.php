<?php

namespace Database\Seeders;

use App\Models\CompanySetting;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CoreDataSeeder extends Seeder
{
    public function run()
    {
        $this->createDefaultSettings();
        $this->createDefaultRolePermissions();
        $this->createDefaultUsers();
    }

    private function createDefaultSettings()
    {
        CompanySetting::create([
            'name'                 => 'Project',
            'mobile1'              => '01xxxxxxxxx',
            'email'                => 'info@gmail.com',
        ]);
    }

    private function createDefaultRolePermissions()
    {

        $permissions = [
            ['name' => 'user.access'],
            ['name' => 'user.create'],
            ['name' => 'user.edit'],
            ['name' => 'user.view'],
            ['name' => 'user.delete'],

            ['name' => 'profile.access'],
            ['name' => 'profile.edit'],

            ['name' => 'settings.access'],
            ['name' => 'settings.edit'],

            ['name' => 'role_permission.access'],
            ['name' => 'role_permission.create'],
            ['name' => 'role_permission.edit'],
            ['name' => 'role_permission.view'],
            ['name' => 'role_permission.delete'],

            ['name' => 'slider.access'],
            ['name' => 'slider.create'],
            ['name' => 'slider.edit'],
            ['name' => 'slider.view'],
            ['name' => 'slider.delete'],

            ['name' => 'team.access'],
            ['name' => 'team.create'],
            ['name' => 'team.edit'],
            ['name' => 'team.view'],
            ['name' => 'team.delete'],

            ['name' => 'league.access'],
            ['name' => 'league.create'],
            ['name' => 'league.edit'],
            ['name' => 'league.view'],
            ['name' => 'league.delete'],

            ['name' => 'match_team.access'],
            ['name' => 'match_team.create'],
            ['name' => 'match_team.edit'],
            ['name' => 'match_team.view'],
            ['name' => 'match_team.delete'],
            ['name' => 'match.live'],
            ['name' => 'match.completed'],
            ['name' => 'match.history'],
            ['name' => 'announce.players'],


            ['name' => 'contest.access'],
            ['name' => 'contest.create'],
            ['name' => 'contest.edit'],
            ['name' => 'contest.view'],
            ['name' => 'contest.delete'],
            ['name' => 'contest.rank.list'],
            ['name' => 'contest.rank.store'],
            ['name' => 'contest.rank.update'],
            ['name' => 'contest.team.complete'],
            ['name' => 'contest.history'],
            ['name' => 'contest.running'],
            ['name' => 'points.update'],

            ['name' => 'register-offer.access'],
            ['name' => 'register-offer.create'],
            ['name' => 'register-offer.edit'],
            ['name' => 'register-offer.view'],
            ['name' => 'register-offer.delete'],

            ['name' => 'package.access'],
            ['name' => 'package.create'],
            ['name' => 'package.edit'],
            ['name' => 'package.view'],
            ['name' => 'package.delete'],


            ['name' => 'payment.access'],
            ['name' => 'payment.create'],
            ['name' => 'payment.edit'],
            ['name' => 'payment.view'],
            ['name' => 'payment.delete'],
            ['name' => 'payment.cancel'],
            ['name' => 'payment.approve'],
            ['name' => 'payment.cancel.list'],


            ['name' => 'player.access'],
            ['name' => 'player.create'],
            ['name' => 'player.edit'],
            ['name' => 'player.view'],
            ['name' => 'player.delete'],
            ['name' => 'player.team.create'],
            ['name' => 'player.team.store'],

            ['name' => 'withdraw.access'],
            ['name' => 'withdraw.create'],
            ['name' => 'withdraw.edit'],
            ['name' => 'withdraw.view'],
            ['name' => 'withdraw.delete'],

            ['name' => 'admin_payroll.access'],
            ['name' => 'admin_payroll.create'],
            ['name' => 'admin_payroll.edit'],
            ['name' => 'admin_payroll.view'],
            ['name' => 'admin_payroll.delete'],

            ['name' => 'promocode.access'],
            ['name' => 'promocode.create'],
            ['name' => 'promocode.edit'],
            ['name' => 'promocode.view'],
            ['name' => 'promocode.delete'],





        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission["name"]
            ]);
        };

        //get all permissions via Gate::before rule; see AuthServiceProvider
        Role::create([
            'name' => 'Super Admin'
        ]);

        $userPermissions = [
            'profile.access',
            'profile.edit',
        ];

        Role::create([
            'name' => 'User'
        ])->givePermissionTo($userPermissions);
    }

    private function createDefaultUsers()
    {
        User::create([
            'username'              => 'admin1',
            'email'             => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('123123123'),
            'remember_token'    => Str::random(10),
        ])->assignRole('Super Admin');
    }
}
