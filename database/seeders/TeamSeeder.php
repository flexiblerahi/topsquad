<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function PHPSTORM_META\map;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([[
            'name' => 'Bangladesh',
            'image' => null,
            'shortname' => 'BD',
        ],[
            'name' => 'India',
            'image' => null,
            'shortname' => 'IND',
        ],[
            'name' => 'Australia',
            'image' => null,
            'shortname' => 'AUS',
        ],[
            'name' => 'New Zealand',
            'image' => null,
            'shortname' => 'NZ',
        ],[
            'name' => 'West Indies',
            'image' => null,
            'shortname' => 'WND',
        ],[
            'name' => 'Sri Lanka',
            'image' => null,
            'shortname' => 'SR',
        ],[
            'name' => 'Afghanistan',
            'image' => null,
            'shortname' => 'AFG',
        ],[
            'name' => 'England',
            'image' => null,
            'shortname' => 'ENG',
        ],[
            'name' => 'Ireland',
            'image' => null,
            'shortname' => 'IRE',
        ],[
            'name' => 'Zimbabwe',
            'image' => null,
            'shortname' => 'ZME',
        ],[
            'name' => 'Pakistan',
            'image' => null,
            'shortname' => 'PAK',
        ],[
            'name' => 'South Africa',
            'image' => null,
            'shortname' => 'SAF',
        ],[
            'name' => 'Mumbai Indians',
            'image' => null,
            'shortname' => 'MI',
        ],[
            'name' => 'Chennai Super Kings',
            'image' => null,
            'shortname' => 'CSK',
        ],[
            'name' => 'Delhi Capitals',
            'image' => null,
            'shortname' => 'DC',
        ],[
            'name' => 'Punjab Kings',
            'image' => null,
            'shortname' => 'PK',
        ],[
            'name' => 'Kolkata Knight Riders',
            'image' => null,
            'shortname' => 'KKR',
        ],[
            'name' => 'Rajasthan Royals',
            'image' => null,
            'shortname' => 'RJ',
        ],[
            'name' => 'Royal Challengers Bangalore',
            'image' => null,
            'shortname' => 'RCB',
        ],[
            'name' => 'Sunrisers Hyderabad',
            'image' => null,
            'shortname' => 'SHR',
        ],[
            'name' => 'Chattogram Challengers',
            'image' => null,
            'shortname' => 'ctg',
        ],[
            'name' => 'Fortune Barishal',
            'image' => null,
            'shortname' => 'FB',
        ],[
            'name' => 'Comilla Victorians',
            'image' => null,
            'shortname' => 'CV',
        ],[
            'name' => 'Dhaka Stars',
            'image' => null,
            'shortname' => 'DS',
        ],[
            'name' => 'Khulna Tigers',
            'image' => null,
            'shortname' => 'KT',
        ],[
            'name' => 'Sylhet SunRisers',
            'image' => null,
            'shortname' => 'SYS',
        ],[
            'name' => 'Rajshahi Royals',
            'image' => null,
            'shortname' => 'RJR',
        ],[
            'name' => 'Rangpur Rangers',
            'image' => null,
            'shortname' => 'RNR',
        ],[
            'name' => 'Barbados Royals',
            'image' => null,
            'shortname' => 'BR',
        ],[
            'name' => 'Guyana Amazon Warriors',
            'image' => null,
            'shortname' => 'GAW',
        ],[
            'name' => 'Jamaica Tallawahs',
            'image' => null,
            'shortname' => 'JT',
        ],[
            'name' => 'St Kitts & Nevis Patriots',
            'image' => null,
            'shortname' => 'SNP',
        ],[
            'name' => 'St Lucia Kings',
            'image' => null,
            'shortname' => 'SLK',
        ],[
            'name' => 'Trinbago Knight Riders',
            'image' => null,
            'shortname' => 'TKR',
        ],[
            'name' => 'Islamabad United',
            'image' => null,
            'shortname' => 'ISU',
        ],[
            'name' => 'Karachi Kings',
            'image' => null,
            'shortname' => 'KK',
        ],[
            'name' => 'Lahore Qalandars',
            'image' => null,
            'shortname' => 'LQ',
        ],[
            'name' => 'Multan Sultans',
            'image' => null,
            'shortname' => 'MS',
        ],[
            'name' => 'Peshawar Zalmi',
            'image' => null,
            'shortname' => 'PZ',
        ],[
            'name' => 'Quetta Gladiators',
            'image' => null,
            'shortname' => 'QG',
        ]]);

    }
}
