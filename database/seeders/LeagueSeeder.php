<?php

namespace Database\Seeders;

use App\Models\League;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeagueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leagues')->insert([[
            'name' => 'International Cricket Council',
            'image' => null,
            'shortname' => 'ICC'
        ],[
            'name' => 'Indian Premier League',
            'image' => null,
            'shortname' => 'IPL'
        ],[
            'name' => 'Bangladesh Premier League',
            'image' => null,
            'shortname' => 'BPL'
        ],[
            'name' => 'Pakistan Premier League',
            'image' => null,
            'shortname' => 'PSL'
        ],[
            'name' => 'Caribbean Premier League',
            'image' => null,
            'shortname' => 'CPL'
        ]]);
    }
}
